/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/28 14:21:30 by mbeilles          #+#    #+#             */
/*   Updated: 2019/11/02 16:06:40 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "compiler.h"
#include "ast.h"
#include "parser.h"
#include "print.h"
#include "dynarray.h"
#include "libft.h"

static inline void	_print_end_state(bool valid, t_state state)
{
	char	*str[2];

	str[0] = ft_strdup(ft_ultostr(state.warnings, 10, 1));
	str[1] = ft_strdup(ft_ultostr(state.errors, 10, 1));
	if (!valid)
		print((t_print){
				.flags = (t_print_flags){.no_exit = true},
				.printer = compiler_printer, .level = LOG_CRIT,
				.data = &(t_print_compiler){
					.no_print_location = true,
					.file_path = "corepiler",
					.message = "\e[0mno input files."}});
	else
		print((t_print){
				.flags = (t_print_flags){.no_exit = true},
				.printer = compiler_printer, .level = LOG_INFO,
				.destructor = compiler_printer_free,
				.data = &(t_print_compiler){
				.no_print_location = true,
				.file_path = "corepiler",
				.message = ft_strajoin(5, "\e[0mCompilation of all files ended with ",
						str[0], (state.warnings > 1)
						? " warnings and " : " warning and ",
						str[1], (state.errors > 1)
						? " errors." : " error.")}});
	ft_afree(2, str[0], str[1]);
}

static inline void	_print_failed_file_open(char *path)
{
	print((t_print){
			.flags = (t_print_flags){.no_exit = true},
			.printer = compiler_printer, .level = LOG_CRIT,
			.destructor = &compiler_printer_free,
			.data = &(t_print_compiler){
			.no_print_location = true,
			.file_path = "corepiler",
			.message = ft_strajoin(3,
					"\e[0mNo such file or directory: '", path, "'.")
			}
	});
}

static inline char	*_change_extension(char *path)
{
	char			*new;
	const char		*ext;

	if (!(ext = ft_strrstr(path, ".")))
		ext = path + ft_strlen(path);
	if (!(new = malloc(ext - path + 5)))
		return ("?.cor");
	ft_memcpy(new, path, ext - path);
	new[ext - path + 0] = '.';
	new[ext - path + 1] = 'c';
	new[ext - path + 2] = 'o';
	new[ext - path + 3] = 'r';
	new[ext - path + 4] = '\0';
	return (new);
}

int					main(int c, char **v) {
	t_dynarray		tokens = ft_dynarray_create_loc(0, 0); // Tokens list
	t_state			state; // Steps return states
	t_binary		binary; // Temporary binary used for transmitting infos
	int				i; // Iterator on argv
	bool			valid; // Checks if a valid file has been given
	t_ast			ast; // ast used for binary syntax and building

	i = 1;
	valid = false;
	while (i < c) {
		// File init
		valid = true;
		binary = (t_binary){
			.source_path = v[i],
			.output_path = _change_extension(v[i]),
			.code = ft_dynarray_create_loc(0, 0),
		};
		print((t_print){
				.flags = (t_print_flags){.no_exit = true},
				.printer = compiler_printer, .level = LOG_INFO,
				.data = &(t_print_compiler){
					.no_print_location = true,
					.file_path = v[i],
					.message = "\e[0mstarting compilation..."
				}
		});
		// Getting file source content
		if (!(binary.source_content = ft_get_file_content(v[i]))) {
			_print_failed_file_open(v[i++]);
			continue ;
		}
		// Tokenize file
		if ((state = tokenize(&tokens, &binary)).error)
		{
			i++;
			ft_dynarray_purge(&tokens);
			free(binary.source_content);
			free(binary.output_path);
			continue ;
		}
		/* ft_dynarray_print(&tokens); */
		// Create ast from tokens
		if (!state.error && (ast = construct_ast(&binary, &tokens)).state.error)
		{
			i++;
			ft_dynarray_purge(&tokens);
			ft_dynarray_destroy(&binary.code, false);
			ast_destroy(&ast);
			free(binary.source_content);
			free(binary.output_path);
			continue ;
		}

		state = (t_state){
			.error = state.error || ast.state.error,
			.errors = state.errors + ast.state.errors,
			.warnings = state.warnings + ast.state.warnings,
		};
		// TODO Parsing from ast
		/* state = parse(&tokens, &binary); */

		// Cleanup
		ft_dynarray_purge(&tokens);
		ft_dynarray_destroy(&binary.code, false);
		ast_destroy(&ast);
		free(binary.source_content);
		// Error handling
		if (state.error)
			print((t_print){
					.flags = (t_print_flags){.no_exit = true},
					.printer = compiler_printer, .level = LOG_CRIT,
					.data = &(t_print_compiler){
						.no_print_location = true,
						.file_path = v[i],
						.message = (state.errors > 1)
							? "\e[0mcompilation aborted due to previous errors."
							: "\e[0mcompilation aborted due to previous error."
						}
					});
		else
			print((t_print){
					.flags = (t_print_flags){.no_exit = true},
					.destructor = compiler_printer_free,
					.printer = compiler_printer, .level = LOG_SUCCESS,
					.data = &(t_print_compiler){
						.no_print_location = true,
						.file_path = v[i],
						.message = ft_strajoin(3, "compiled at '\e[4m", binary.output_path, "\e[0m'.")
					}
			});
		i++;
	}
	// Error display
	_print_end_state(valid, state);
	// Last cleanup
	ft_dynarray_destroy(&tokens, false);
}
