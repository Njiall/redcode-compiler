/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   destroy.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: njiall <mbeilles@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/18 18:38:49 by njiall            #+#    #+#             */
/*   Updated: 2019/10/19 04:37:56 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ast.h"
#include "dynarray.h"

static void		_expr_destroy(t_expr *expr)
{
	for (int i = 0; i < expr->len; i++)
		_expr_destroy(expr->args + i);
	if (expr->args)
		free(expr->args);
}

static void		_ast_node_destroy(t_ast_node *node)
{
	if (node && node->type == AST_TYPE_INSTRUCTION)
	{
		for (int i = 0; i < node->instruction.len; i++)
			_ast_node_destroy(node->instruction.args + i);
		free(node->instruction.args);
	}
	else if (node->type == AST_TYPE_EXPR)
	{
		_expr_destroy(&node->expr.expr);
	}
}

void			ast_destroy(t_ast *ast)
{
	t_ast_node	*node;
	uint64_t	i;

	i = 0;
	while ((node = ft_dynarray_iterate(&ast->tree, &i, sizeof(t_ast_node))))
		_ast_node_destroy(node);
	ft_dynarray_destroy(&ast->tree, false);
}
