/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   construct.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 16:21:42 by mbeilles          #+#    #+#             */
/*   Updated: 2019/11/02 19:57:03 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ast.h"
#include "print.h"
#include "dynarray.h"

// Serve the purpose of skipping delimitter tokens like newlines
static inline t_ast_construct	_skip(
		t_ast_constructor_param param
)
{
	return ((t_ast_construct){});
}

// Serve the purpose of skipping tokens until a newline
static inline void				_skip_line(
		t_dynarray *tokens,
		uint64_t *i
)
{
	t_token						*token;

	while ((token = ft_dynarray_iterate(tokens, i, sizeof(t_token)))
			&& token->type != TOK_NEWLINE);
}

t_ast_construct			construct_key(
		t_ast_constructor_param param
)
{
	bool				warn;
	t_ast_key			*tmp;

	size_t				scope_before;
	size_t				scope_after;

	if (ft_hashmap_get(&param.ast->keys, param.input->str, param.input->len, (const void **)&tmp)) {

		print((t_print){
				.level = LOG_WARN,
				.printer = compiler_printer,

				.data = &(t_print_compiler){
					.target = param.input,
					.conflict = tmp->token,
					.file = param.binary->source_content,
					.file_path = param.binary->source_path,
					.message = "key redefinition. [Wkey-redefine]"
				}

			}
		);
		warn = true;
	}
	if (!(tmp = malloc(sizeof(t_ast_key))))
		ft_exit(1, "No space left on device.\n", NULL);
	*tmp = (t_ast_key){
			.type = AST_TYPE_KEY,
			.key = param.input->str,
			.len = param.input->len,
			.token = param.input,
	};
	ft_hashmap_set(&param.ast->keys, param.input->str, param.input->len, tmp);
	ft_dynarray_push(&param.ast->tree, &(t_ast_node){.key = *tmp}, sizeof(t_ast_node));

	return ((t_ast_construct){
			.state = (t_state){
				.warnings = warn,
			},
			.node = (t_ast_node){
				.token = (t_ast_token){
					.type = AST_TYPE_KEY,
					.token = *param.input,
				},
			},
	});
}

t_ast_construct			(*g_sub_constructor[TOK_MAX])(
		t_ast_constructor_param param
) = {
	[TOK_WORD] = construct_instruction,
	[TOK_PROP] = construct_property,
	[TOK_KEY_REF] = construct_key,
	[TOK_NEWLINE] = _skip,
};

static char				*g_token_types[TOK_MAX] = {
	[TOK_NONE] = 			"none",
	[TOK_WORD] = 			"\e[1;34mword\e[0m",
	[TOK_NUMBER] = 			"\e[1;33mnumber\e[0m",
	[TOK_DIRECT_NUMBER] = 	"\e[1;33mdirect number\e[0m",
	[TOK_NEWLINE] = 		"\e[1;35mnewline\e[0m",
	[TOK_SEPARATOR] = 		"\e[1;32mseparator\e[0m",
	[TOK_INDIRECT_REF] = 	"\e[1;36mindirect reference\e[0m",
	[TOK_DIRECT_REF] = 		"\e[1;36mdirect reference\e[0m",
	[TOK_KEY_REF] = 		"\e[1;36mkey\e[0m",
	[TOK_PROP] = 			"\e[1;31mproperty\e[0m",
	[TOK_STRING] = 			"\e[1;31mstring\e[0m",
	[TOK_REGISTER] = 		"\e[1;34mregister\e[0m",
	[TOK_OP_ADD] = 			"\e[1;32madd\e[0m",
	[TOK_OP_SUB] = 			"\e[1;32msub\e[0m",
	[TOK_OP_DIV] = 			"\e[1;32mdivide\e[0m",
	[TOK_OP_MULT] = 		"\e[1;32mmult\e[0m",

	[TOK_OP_AND_LOG] = 		"\e[1;32mand \e[0;32mlogic\e[0m",
	[TOK_OP_XOR_LOG] = 		"\e[1;32mxor \e[0;32mlogic\e[0m",
	[TOK_OP_OR_LOG] = 		"\e[1;32mor \e[0;32mlogic\e[0m",
	[TOK_OP_AND_BIT] = 		"\e[1;32mand \e[0;32mbitwise\e[0m",
	[TOK_OP_XOR_BIT] = 		"\e[1;32mxor \e[0;32mbitwise\e[0m",
	[TOK_OP_OR_BIT] = 		"\e[1;32mor \e[0;32mbitwise\e[0m",

	[TOK_OP_LSHIFT] = 		"\e[32mleft bit \e[1mshift\e[0m",
	[TOK_OP_RSHIFT] = 		"\e[32mright bit \e[1mshift\e[0m",

	[TOK_OP_NOT] = 			"\e[1;32mnot\e[0m",
	[TOK_EXPR_BRC_CLOSE] = 	"\e[1;32mclose\e[0;32m expr\e[0m",
	[TOK_EXPR_BRC_OPEN] = 	"\e[1;32mopen\e[0;32m expr\e[0m",
};

static inline void		print_unexpected_token(
		t_binary *binary,
		t_token *target
)
{
	print((t_print){
			.flags = (t_print_flags){.no_exit = true},

			.level = LOG_CRIT,
			.printer = compiler_printer,
			.destructor = compiler_printer_free,

			.data = &(t_print_compiler){
				.target = target,
				.file = binary->source_content,
				.file_path = binary->source_path,
				.message = ft_strajoin(3, "Unexpected ",
						g_token_types[target->type], ".")
			}

		}
	);
}

static inline t_state	_merge_states(
	t_state s1,
	t_state s2,
	t_token *target
)
{
	return ((t_state){
		.error = s1.error || s2.error,
		.errors = s1.errors + s2.errors,
		.warnings = s1.warnings + s2.warnings,
		.line = target->line,
		.col = target->col,
	});
}

# define ERR_LIMIT 10
t_ast					construct_ast(
		t_binary *binary,
		t_dynarray *tokens
)
{
	t_ast				ast;
	t_token				*token;
	t_ast_construct		branch;
	uint64_t			i;

	ast = (t_ast){
		.tree = ft_dynarray_create_loc(0, 0), // Creating root
		.state = (t_state){}, // Explicit no state
		.keys = ft_hashmap_create_default_loc(),
	};

	// Iteration over tokens to create basic leafs
	// TODO fill tree
	i = 0;
	while ((token = ft_dynarray_iterate(tokens, &i, sizeof(t_token))))
	{
		// Check if there is a tree constructor for the token's type
		if (g_sub_constructor[token->type])
		{
			// Get the branch from the sub constructor
			branch = g_sub_constructor[token->type]((t_ast_constructor_param){
					.ast = &ast,
					.binary = binary,
					.tokens = tokens,
					.input = token,
					.index = &i,
			});
			// Check if the constructor didn't encounter an error
			if (branch.state.error)
			{
				// TODO destroy branch
				_skip_line(tokens, &i);
				break ;
			}
			if (ast.state.errors > ERR_LIMIT)
				break ;
			if (branch.valid) {
				ft_dynarray_push(&ast.tree, &branch.node, sizeof(t_ast_node));
			}
			// Merge the states for handling later
			ast.state = _merge_states(ast.state, branch.state, token);
		}
		else
		{
			print_unexpected_token(binary, token);
			ast.state.error = true;
			ast.state.errors++;
		}
	}
	/* print_ast(&ast); */
	if (ast.state.error)
	{
		// TODO Handle errors
		print((t_print){
				.flags = (t_print_flags){.no_exit = true},
				.level = LOG_CRIT,
				.printer = compiler_printer,
				.data = &(t_print_compiler){
					.message = "There was an error generating the ast.",
					.file_path = binary->source_path,
					.file = binary->source_content,
				},
		});
		return (ast);
	}
	return (ast);
}
