/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   construct_instruction.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: njiall <mbeilles@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/09 17:01:53 by njiall            #+#    #+#             */
/*   Updated: 2019/11/02 16:23:45 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "compiler.h"
#include "ast.h"
#include "parser.h"
#include "print.h"
#include "libft.h"

static char						*g_token_types[TOK_MAX] = {
	[TOK_NONE] = 			"none",
	[TOK_WORD] = 			TOK_STR_WORD,
	[TOK_NUMBER] = 			TOK_STR_INDIRECT,
	[TOK_DIRECT_NUMBER] = 	TOK_STR_DIRECT_NUMBER,
	[TOK_NEWLINE] = 		TOK_STR_NEWLINE,
	[TOK_SEPARATOR] = 		TOK_STR_SEPARATOR,
	[TOK_INDIRECT_REF] = 	TOK_STR_INDIRECT_REF,
	[TOK_DIRECT_REF] = 		TOK_STR_DIRECT_REF,
	[TOK_KEY_REF] = 		TOK_STR_KEY_REF,
	[TOK_PROP] = 			TOK_STR_PROP,
	[TOK_STRING] = 			TOK_STR_STRING,
	[TOK_REGISTER] = 		TOK_STR_REGISTER,

	[TOK_OP_ADD] = 			TOK_STR_ADD,
	[TOK_OP_SUB] = 			TOK_STR_SUB,
	[TOK_OP_DIV] = 			TOK_STR_DIV,
	[TOK_OP_MULT] = 		TOK_STR_MULT,

	[TOK_OP_AND_LOG] = 		TOK_STR_AND_BIT,
	[TOK_OP_XOR_LOG] = 		TOK_STR_XOR_BIT,
	[TOK_OP_OR_LOG] = 		TOK_STR_OR_BIT,
	[TOK_OP_AND_BIT] = 		TOK_STR_AND_LOG,
	[TOK_OP_XOR_BIT] = 		TOK_STR_XOR_LOG,
	[TOK_OP_OR_BIT] = 		TOK_STR_OR_LOG,

	[TOK_OP_LSHIFT] = 		TOK_STR_LSHIFT,
	[TOK_OP_RSHIFT] = 		TOK_STR_RSHIFT,

	[TOK_OP_NOT] = 			TOK_STR_NOT,
	[TOK_EXPR_BRC_CLOSE] = 	TOK_STR_EXPR_BRC_CLOSE,
	[TOK_EXPR_BRC_OPEN] = 	TOK_STR_EXPR_BRC_OPEN,
};

static char						*g_states[PARSER_MAX] = {
	[PARSER_DEFAULT_ERR] = 			"default error",
	[PARSER_ERR_NSP_A1] = 			"arg 2 no sep",
	[PARSER_ERR_NSP_A2] = 			"arg 3 no sep",
	[PARSER_ERR_NSP_A3] = 			"arg 4 no sep",
	[PARSER_ERR_WRONG_TYPE_NC] = 	"wrong type // auto cast",
	[PARSER_ERR_UNDEFINED_OP] = 	"not an op",
	[PARSER_ERR_WRONG_REG] = 		"not a register",
	[PARSER_ERR_NOT_ENOUGH_ARGS] = 	"not enough args",
	[PARSER_ERR_TOO_MANY_ARGS] = 	"too many args",
	[PARSER_ERR_WRONG_TYPE_C] = 	"wrong type // no cast",
	[PARSER_MAX_ERR] = 				"err max // shouldn't happen",
	[PARSER_INIT] = 				"init",
	[PARSER_OP_START] = 			"start",
	[PARSER_OP_ARG_1] = 			"arg 1",
	[PARSER_OP_ARG_2] = 			"arg 2",
	[PARSER_OP_ARG_3] = 			"arg 3",
	[PARSER_OP_ARG_4] = 			"arg 4",
	[PARSER_OP_ARG_1_S] = 			"sep 1",
	[PARSER_OP_ARG_2_S] = 			"sep 2",
	[PARSER_OP_ARG_3_S] = 			"sep 3",
	[PARSER_EXPR] = 				"expression",
	// [Miscs] = ""
	[PARSER_KEY] = 					"key",
	[PARSER_EXEC] = 				"exec",
	[PARSER_SKIP] = 				"skip",
};

static char						*g_op_arg_types[OP_MAX][PARSER_MAX] = {
	[OP_BOOL] = {
		// Arg 1
		[PARSER_OP_START] = TOK_STR_REGISTER", "TOK_STR_DIRECT" or "TOK_STR_INDIRECT,
		// Arg 2
		[PARSER_OP_ARG_1] = TOK_STR_REGISTER", "TOK_STR_DIRECT" or "TOK_STR_INDIRECT,
		[PARSER_OP_ARG_1_S] = TOK_STR_REGISTER", "TOK_STR_DIRECT" or "TOK_STR_INDIRECT,
		// Arg 3
		[PARSER_OP_ARG_2] = TOK_STR_REGISTER,
		[PARSER_OP_ARG_2_S] = TOK_STR_REGISTER,
		[PARSER_ERR_NSP_A1] = TOK_STR_REGISTER,
	},
	[OP_LOGIC] = {
		[PARSER_OP_ARG_1] = "test 1",
		[PARSER_OP_ARG_2] = "test 2",
		[PARSER_ERR_NSP_A1] = "m test 1",
		[PARSER_ERR_NSP_A2] = "m test 2",
	},
	[OP_LIVE] = {
		[PARSER_OP_START] = TOK_STR_DIRECT,
	},
	[OP_ST] = {
		[PARSER_OP_ARG_1] = "a \e[1;34mregister\e[0;1m",
		[PARSER_OP_ARG_2] = "an indirect or a register",
		[PARSER_ERR_NSP_A1] = "an indirect or a register",
	},
	[OP_NONE] = {
		[PARSER_DEFAULT_ERR ... PARSER_MAX - 1] = " ",
	}
};

static char						*g_error_known[PARSER_MAX_ERR] = {
	[PARSER_DEFAULT_ERR] = "unexpected token.",
	[PARSER_ERR_NSP_A1] = "missing separator after first arg. \e[1;30m[\e[0;35m-Wno-separator\e[0;1;30m]\e[0m",
	[PARSER_ERR_NSP_A2] = "missing separator after second arg. \e[1;30m[\e[0;35m-Wno-separator\e[0;1;30m]\e[0m",
	[PARSER_ERR_NSP_A3] = "missing separator after third arg. \e[1;30m[\e[0;35m-Wno-separator\e[0;1;30m]\e[0m",
	[PARSER_ERR_NOT_ENOUGH_ARGS] = "not enough args for <op>.",
	[PARSER_ERR_TOO_MANY_ARGS] = "too many arguments for <op>. \e[1;30m[\e[0;35m-Eargs-overflow\e[0;1;30m]\e[0m",
	[PARSER_ERR_UNDEFINED_OP] = "no \"<op>\" instruction known. \e[1;30m[\e[0;35m-Enoop\e[0;1;30m]\e[0m",
	[PARSER_ERR_WRONG_TYPE_C] = "<op> requires <types> but '<tok>' is <type>. \e[1;30m[\e[0;35m-Wcast\e[0;1;30m]\e[0m",
	[PARSER_ERR_WRONG_TYPE_NC] = "<op> requires <types> but '<tok>' is <type>, casting auto. \e[1;30m[\e[0;35m-Wauto-cast\e[0;1;30m]\e[0m",
	[PARSER_ERR_WRONG_REG] = "'<tok>' isn't "TOK_STR_REGISTER". Did you mean \e[4m<suggest>\e[0;1m?. \e[1;30m[\e[0;35m-Wabstract-registers\e[0;1;30m]\e[0m",
};

static char						*g_token_states[PARSER_MAX] = {
	// Error states
	[PARSER_DEFAULT_ERR] = 				"error",
	[PARSER_ERR_UNDEFINED_OP] = 		"undefined op",
	[PARSER_ERR_TOO_MANY_ARGS] = 		"too many",
	[PARSER_ERR_NOT_ENOUGH_ARGS] = 		"not enough",
	[PARSER_ERR_NSP_A1] = 				"no sep arg 1",
	[PARSER_ERR_NSP_A2] = 				"no sep arg 2",
	[PARSER_ERR_NSP_A3] = 				"no sep arg 3",
	[PARSER_ERR_WRONG_TYPE_C] = 		"wrong type (crit)",
	[PARSER_ERR_WRONG_TYPE_NC] = 		"wrong type (warn)",
	// Parsing states
	[PARSER_INIT] = 					"init",
	[PARSER_OP_START] = 				"start of op",
	[PARSER_OP_ARG_1] = 				"arg 1",
	[PARSER_OP_ARG_2] = 				"arg 2",
	[PARSER_OP_ARG_3] = 				"arg 3",
	[PARSER_OP_ARG_4] = 				"arg 4",
	[PARSER_OP_ARG_1_S] = 				"arg 1 separator",
	[PARSER_OP_ARG_2_S] = 				"arg 2 separator",
	[PARSER_OP_ARG_3_S] = 				"arg 3 separator",
	[PARSER_SKIP] = 					"skip",
	[PARSER_EXEC] = 					"exec",
};

static bool						g_error_critical[PARSER_MAX_ERR] = {
	[PARSER_DEFAULT_ERR] = true,
	[ERR_CRIT] = true,
};

static t_tok_state				g_parse_tree[OP_MAX][PARSER_MAX][TOK_MAX] = {
	[OP_NONE] = {
		[PARSER_ANY][TOK_ANY] = PARSER_ERR_UNDEFINED_OP,
	},
	[OP_LIVE] = {
		// Valid states
		[PARSER_OP_START][TOK_DIRECT_REF] = PARSER_OP_ARG_1,
		[PARSER_OP_START][TOK_DIRECT_NUMBER] = PARSER_OP_ARG_1,
		[PARSER_OP_START][TOK_ANY_EXPR] = PARSER_EXPR,

		[PARSER_OP_ARG_1][TOK_NEWLINE] = PARSER_EXEC,
		[PARSER_ERR_WRONG_TYPE_NC][TOK_NEWLINE] = PARSER_EXEC,
		// Errors
		// 		Forgotten args
		[PARSER_OP_START][TOK_NEWLINE] = PARSER_ERR_NOT_ENOUGH_ARGS,
		// 		Too many args
		[PARSER_OP_ARG_1][TOK_SEPARATOR] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_1][TOK_REGISTER] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_1][TOK_NUMBER] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_1][TOK_INDIRECT_REF] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_1][TOK_DIRECT_REF] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_1][TOK_DIRECT_NUMBER] = PARSER_ERR_TOO_MANY_ARGS,
		// 		Wrong type
		[PARSER_OP_START][TOK_REGISTER] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_START][TOK_INDIRECT_REF] = PARSER_ERR_WRONG_TYPE_NC,
		[PARSER_OP_START][TOK_NUMBER] = PARSER_ERR_WRONG_TYPE_NC,
	},
	[OP_BOOL] = {
		// Valid states
		// 		Arg 1
		[PARSER_OP_START][TOK_REGISTER] = PARSER_OP_ARG_1,
		[PARSER_OP_START][TOK_NUMBER] = PARSER_OP_ARG_1,
		[PARSER_OP_START][TOK_INDIRECT_REF] = PARSER_OP_ARG_1,
		[PARSER_OP_START][TOK_DIRECT_NUMBER] = PARSER_OP_ARG_1,
		[PARSER_OP_START][TOK_DIRECT_REF] = PARSER_OP_ARG_1,
		// 		Arg 1 separator
		[PARSER_OP_ARG_1][TOK_SEPARATOR] = PARSER_OP_ARG_1_S,
		// 		Arg 2
		[PARSER_OP_ARG_1_S][TOK_REGISTER] = PARSER_OP_ARG_2,
		[PARSER_OP_ARG_1_S][TOK_NUMBER] = PARSER_OP_ARG_2,
		[PARSER_OP_ARG_1_S][TOK_INDIRECT_REF] = PARSER_OP_ARG_2,
		[PARSER_OP_ARG_1_S][TOK_DIRECT_NUMBER] = PARSER_OP_ARG_2,
		[PARSER_OP_ARG_1_S][TOK_DIRECT_REF] = PARSER_OP_ARG_2,
		[PARSER_ERR_NSP_A1][TOK_SEPARATOR] = PARSER_OP_ARG_2_S,
		// 		Arg 2 separator
		[PARSER_OP_ARG_2][TOK_SEPARATOR] = PARSER_OP_ARG_2_S,
		[PARSER_ERR_NSP_A2][TOK_SEPARATOR] = PARSER_OP_ARG_3_S,
		// 		Arg 3
		[PARSER_OP_ARG_2_S][TOK_REGISTER] = PARSER_OP_ARG_3,
		[PARSER_ERR_NSP_A1][TOK_REGISTER] = PARSER_ERR_NSP_A2,
		// 		Validation
		[PARSER_OP_ARG_3][TOK_NEWLINE] = PARSER_EXEC,
		[PARSER_ERR_NSP_A2][TOK_NEWLINE] = PARSER_EXEC,
		[PARSER_ERR_WRONG_TYPE_NC][TOK_NEWLINE] = PARSER_EXEC,

		// Errors
		// 		Forgotten separators
		// 	Arg 1
		[PARSER_OP_ARG_1][TOK_REGISTER] = PARSER_ERR_NSP_A1,
		[PARSER_OP_ARG_1][TOK_NUMBER] = PARSER_ERR_NSP_A1,
		[PARSER_OP_ARG_1][TOK_INDIRECT_REF] = PARSER_ERR_NSP_A1,
		[PARSER_OP_ARG_1][TOK_DIRECT_REF] = PARSER_ERR_NSP_A1,
		// 	Arg2
		[PARSER_OP_ARG_2][TOK_REGISTER] = PARSER_ERR_NSP_A2,
		[PARSER_OP_ARG_2][TOK_NUMBER] = PARSER_ERR_NSP_A2,
		[PARSER_OP_ARG_2][TOK_INDIRECT_REF] = PARSER_ERR_NSP_A2,
		[PARSER_OP_ARG_2][TOK_DIRECT_REF] = PARSER_ERR_NSP_A2,
		[PARSER_OP_ARG_2][TOK_DIRECT_NUMBER] = PARSER_ERR_NSP_A2,
		// 		Wrong register
		// 	Arg 1
		[PARSER_OP_START][TOK_WORD] = PARSER_ERR_WRONG_REG,
		// 	Arg2
		[PARSER_OP_ARG_1][TOK_WORD] = PARSER_ERR_WRONG_REG,
		[PARSER_OP_ARG_1_S][TOK_WORD] = PARSER_ERR_WRONG_REG,
		// 	Arg 3
		[PARSER_OP_ARG_2][TOK_WORD] = PARSER_ERR_WRONG_REG,
		[PARSER_OP_ARG_2_S][TOK_WORD] = PARSER_ERR_WRONG_REG,
		// 		Forgotten args
		[PARSER_OP_START][TOK_NEWLINE] = PARSER_ERR_NOT_ENOUGH_ARGS,
		[PARSER_OP_ARG_1][TOK_NEWLINE] = PARSER_ERR_NOT_ENOUGH_ARGS,
		[PARSER_OP_ARG_2][TOK_NEWLINE] = PARSER_ERR_NOT_ENOUGH_ARGS,
		[PARSER_OP_ARG_1_S][TOK_NEWLINE] = PARSER_ERR_NOT_ENOUGH_ARGS,
		[PARSER_OP_ARG_2_S][TOK_NEWLINE] = PARSER_ERR_NOT_ENOUGH_ARGS,
		[PARSER_ERR_NSP_A1][TOK_NEWLINE] = PARSER_ERR_NOT_ENOUGH_ARGS,
		// 		Too many args
		[PARSER_OP_ARG_3][TOK_SEPARATOR] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_3][TOK_REGISTER] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_3][TOK_NUMBER] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_3][TOK_INDIRECT_REF] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_3][TOK_DIRECT_REF] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_3][TOK_DIRECT_NUMBER] = PARSER_ERR_TOO_MANY_ARGS,
		// 		Wrong type
		[PARSER_OP_START][TOK_STRING] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_1][TOK_STRING] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_2][TOK_STRING] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_1_S][TOK_STRING] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_2_S][TOK_STRING] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_ERR_NSP_A1][TOK_STRING] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_ERR_NSP_A2][TOK_STRING] = PARSER_ERR_WRONG_TYPE_C,

		[PARSER_OP_ARG_2_S][TOK_NUMBER] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_2_S][TOK_INDIRECT_REF] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_2_S][TOK_DIRECT_NUMBER] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_2_S][TOK_DIRECT_REF] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_ERR_NSP_A1][TOK_NUMBER] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_ERR_NSP_A1][TOK_INDIRECT_REF] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_ERR_NSP_A1][TOK_DIRECT_NUMBER] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_ERR_NSP_A1][TOK_DIRECT_REF] = PARSER_ERR_WRONG_TYPE_C,
	},
	[OP_LOGIC] = {
		// Valid states
		// 		Arg 1
		[PARSER_OP_START][TOK_REGISTER] = PARSER_OP_ARG_1,
		[PARSER_OP_ARG_1][TOK_SEPARATOR] = PARSER_OP_ARG_1_S,
		// 		Arg 2
		[PARSER_OP_ARG_1_S][TOK_REGISTER] = PARSER_OP_ARG_2,
		[PARSER_ERR_NSP_A1][TOK_REGISTER] = PARSER_ERR_NSP_A2,
		[PARSER_OP_ARG_2][TOK_SEPARATOR] = PARSER_OP_ARG_2_S,
		[PARSER_ERR_NSP_A1][TOK_SEPARATOR] = PARSER_OP_ARG_2_S,
		// 		Arg 3
		[PARSER_OP_ARG_2_S][TOK_REGISTER] = PARSER_OP_ARG_3,
		[PARSER_ERR_NSP_A2][TOK_SEPARATOR] = PARSER_OP_ARG_3_S,
		// 		Validation
		[PARSER_OP_ARG_3][TOK_NEWLINE] = PARSER_EXEC,
		[PARSER_ERR_NSP_A2][TOK_NEWLINE] = PARSER_EXEC,
		[PARSER_ERR_WRONG_TYPE_NC][TOK_NEWLINE] = PARSER_EXEC,

		// Errors
		// 		Forgotten separators
		// 	Arg 1
		[PARSER_OP_ARG_1][TOK_REGISTER] = PARSER_ERR_NSP_A1,
		[PARSER_OP_ARG_1][TOK_NUMBER] = PARSER_ERR_NSP_A1,
		[PARSER_OP_ARG_1][TOK_INDIRECT_REF] = PARSER_ERR_NSP_A1,
		[PARSER_OP_ARG_1][TOK_DIRECT_REF] = PARSER_ERR_NSP_A1,
		[PARSER_OP_ARG_1][TOK_DIRECT_NUMBER] = PARSER_ERR_NSP_A1,
		// 	Arg2
		[PARSER_OP_ARG_2][TOK_REGISTER] = PARSER_ERR_NSP_A2,
		[PARSER_OP_ARG_2][TOK_NUMBER] = PARSER_ERR_NSP_A2,
		[PARSER_OP_ARG_2][TOK_INDIRECT_REF] = PARSER_ERR_NSP_A2,
		[PARSER_OP_ARG_2][TOK_DIRECT_REF] = PARSER_ERR_NSP_A2,
		[PARSER_OP_ARG_2][TOK_DIRECT_NUMBER] = PARSER_ERR_NSP_A2,
		// 		Forgotten args
		[PARSER_OP_START][TOK_NEWLINE] = PARSER_ERR_NOT_ENOUGH_ARGS,
		[PARSER_OP_ARG_1][TOK_NEWLINE] = PARSER_ERR_NOT_ENOUGH_ARGS,
		[PARSER_OP_ARG_2][TOK_NEWLINE] = PARSER_ERR_NOT_ENOUGH_ARGS,
		[PARSER_OP_ARG_1_S][TOK_NEWLINE] = PARSER_ERR_NOT_ENOUGH_ARGS,
		[PARSER_OP_ARG_2_S][TOK_NEWLINE] = PARSER_ERR_NOT_ENOUGH_ARGS,
		[PARSER_ERR_NSP_A1][TOK_NEWLINE] = PARSER_ERR_NOT_ENOUGH_ARGS,
		// 		Too many args
		[PARSER_OP_ARG_3][TOK_SEPARATOR] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_3][TOK_REGISTER] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_3][TOK_NUMBER] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_3][TOK_INDIRECT_REF] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_3][TOK_DIRECT_REF] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_3][TOK_DIRECT_NUMBER] = PARSER_ERR_TOO_MANY_ARGS,
		// 		Wrong type
		[PARSER_OP_START][TOK_NUMBER] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_START][TOK_INDIRECT_REF] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_START][TOK_DIRECT_NUMBER] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_START][TOK_DIRECT_REF] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_START][TOK_STRING] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_1_S][TOK_NUMBER] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_1_S][TOK_INDIRECT_REF] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_1_S][TOK_DIRECT_NUMBER] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_1_S][TOK_DIRECT_REF] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_1_S][TOK_STRING] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_2_S][TOK_NUMBER] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_2_S][TOK_INDIRECT_REF] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_2_S][TOK_DIRECT_NUMBER] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_2_S][TOK_DIRECT_REF] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_2_S][TOK_STRING] = PARSER_ERR_WRONG_TYPE_C,
	},
	[OP_ST] = {
		// Valid states
		// 		Arg 1
		[PARSER_OP_START][TOK_REGISTER] = PARSER_OP_ARG_1,
		[PARSER_OP_ARG_1][TOK_SEPARATOR] = PARSER_OP_ARG_1_S,
		// 		Arg 2
		[PARSER_OP_ARG_1_S][TOK_REGISTER] = PARSER_OP_ARG_2,
		[PARSER_OP_ARG_1_S][TOK_NUMBER] = PARSER_OP_ARG_2,
		[PARSER_OP_ARG_1_S][TOK_INDIRECT_REF] = PARSER_OP_ARG_2,
		// 		Validation
		[PARSER_OP_ARG_2][TOK_NEWLINE] = PARSER_EXEC,
		[PARSER_ERR_NSP_A1][TOK_NEWLINE] = PARSER_EXEC,
		// Warnings
		// 		Forgotten separators
		[PARSER_OP_ARG_1][TOK_REGISTER] = PARSER_ERR_NSP_A1,
		[PARSER_OP_ARG_1][TOK_NUMBER] = PARSER_ERR_NSP_A1,
		[PARSER_OP_ARG_1][TOK_INDIRECT_REF] = PARSER_ERR_NSP_A1,
		// Errors
		// 		Wrong type
		[PARSER_OP_START][TOK_NUMBER] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_START][TOK_INDIRECT_REF] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_START][TOK_DIRECT_NUMBER] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_START][TOK_DIRECT_REF] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_1][TOK_DIRECT_NUMBER] = PARSER_ERR_WRONG_TYPE_C,
		[PARSER_OP_ARG_1][TOK_DIRECT_REF] = PARSER_ERR_WRONG_TYPE_C,
		// 		Forgotten args
		[PARSER_OP_START][TOK_NEWLINE] = PARSER_ERR_NOT_ENOUGH_ARGS,
		[PARSER_OP_ARG_1][TOK_NEWLINE] = PARSER_ERR_NOT_ENOUGH_ARGS,
		[PARSER_OP_ARG_1_S][TOK_NEWLINE] = PARSER_ERR_NOT_ENOUGH_ARGS,
		[PARSER_OP_ARG_2_S][TOK_NEWLINE] = PARSER_ERR_NOT_ENOUGH_ARGS,
		// Too many args
		[PARSER_OP_ARG_2][TOK_SEPARATOR] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_2][TOK_REGISTER] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_2][TOK_NUMBER] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_2][TOK_INDIRECT_REF] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_2][TOK_DIRECT_REF] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_OP_ARG_2][TOK_DIRECT_NUMBER] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_ERR_NSP_A1][TOK_SEPARATOR] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_ERR_NSP_A1][TOK_REGISTER] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_ERR_NSP_A1][TOK_NUMBER] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_ERR_NSP_A1][TOK_INDIRECT_REF] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_ERR_NSP_A1][TOK_DIRECT_REF] = PARSER_ERR_TOO_MANY_ARGS,
		[PARSER_ERR_NSP_A1][TOK_DIRECT_NUMBER] = PARSER_ERR_TOO_MANY_ARGS,
	},
};

static char						*g_keyword[OP_MAX] = {
	[OP_NONE] = "none",
	[OP_NOPE] = "noop",
	[OP_LIVE] = "live",

	[OP_LD] = "ld",
	[OP_ST] = "st",

	[OP_ADD] = "add",
	[OP_SUB] = "sub",

	[OP_AND] = "and",
	[OP_XOR] = "xor",
	[OP_OR] = "or",
};

static inline t_op_type			_get_op(t_token *token)
{
	t_op_type					i;

	i = OP_NONE;
	while (++i < OP_MAX) {
		if (strlen(g_keyword[i]) == token->word.len
				&& !strncmp(token->word.str, g_keyword[i], token->word.len)) {
			return (i);
		}
	}
	return (OP_NONE);
}

char							*replace_format_placeholders(
		char *error_message,
		int number,
		...
)
{
	t_print_pattern		pp;
	va_list				patterns;
	const char			*pattern;
	char				*message;
	t_dynarray			str;

	va_start(patterns, number);
	str = ft_dynarray_create_loc(0, 0);
	ft_dynarray_push_str(&str, error_message);
	ft_dynarray_push(&str, "", 1);

	while (--number >= 0)
	{
		pp = va_arg(patterns, t_print_pattern);
		while ((pattern = ft_strstr((const char *)str.array, pp.pattern)))
		{
			ft_dynarray_remove(&str, pattern - (char*)(str.array + str.offset), pp.plen);
			ft_dynarray_insert(&str, pattern - (char*)(str.array + str.offset), pp.replace, pp.rlen);
		}
	}
	ft_dynarray_push(&str, "\e[0m\n", 5);
	message = ft_dynarray_to_str(&str);
	ft_dynarray_destroy(&str, false);
	return (message);
}

static inline char				*_get_formated_error(
		t_parse_state_machine *m,
		t_token *token,
		char *file_path
)
{
	char						*suggestion;

	if (token->type == TOK_WORD) {
		suggestion = get_suggestion(token->word.str, token->word.len,
				(t_string_list){
					.len = 32,
					.list = {
						{.str = "r1", .len = 2},
						{.str = "r2", .len = 2},
						{.str = "r3", .len = 2},
						{.str = "r4", .len = 2},
						{.str = "r5", .len = 2},
						{.str = "r6", .len = 2},
						{.str = "r7", .len = 2},
						{.str = "r8", .len = 2},
						{.str = "r9", .len = 2},
						{.str = "r01", .len = 3},
						{.str = "r02", .len = 3},
						{.str = "r03", .len = 3},
						{.str = "r04", .len = 3},
						{.str = "r05", .len = 3},
						{.str = "r06", .len = 3},
						{.str = "r07", .len = 3},
						{.str = "r08", .len = 3},
						{.str = "r09", .len = 3},
						{.str = "r10", .len = 3},
						{.str = "r11", .len = 3},
						{.str = "r12", .len = 3},
						{.str = "r13", .len = 3},
						{.str = "r14", .len = 3},
						{.str = "r15", .len = 3},
						{.str = "r16", .len = 3},
						{.str = "r010", .len = 4},
						{.str = "r011", .len = 4},
						{.str = "r012", .len = 4},
						{.str = "r013", .len = 4},
						{.str = "r014", .len = 4},
						{.str = "r015", .len = 4},
						{.str = "r016", .len = 4},
					}
				}
		);
	}
	else
		suggestion = NULL;

	/* printf("prev: %s\n", g_token_states[m->next_state]); */
	/* printf("strlen: %lu\n", ft_strlen(g_error_known[m->next_state])); */

	return (replace_format_placeholders(g_error_known[m->next_state],
			5,
			(t_print_pattern){
				.pattern = "<op>", .plen = 4,
				.replace = m->op_token->word.str,
				.rlen = m->op_token->word.len
			},
			(t_print_pattern){
				.pattern = "<types>", .plen = 7,
				.replace = g_op_arg_types[m->type][m->prev_state],
				.rlen = ft_strlen(g_op_arg_types[m->type][m->prev_state]),
			},
			(t_print_pattern){
				.pattern = "<type>", .plen = 6,
				.replace = g_token_types[token->type],
				.rlen = ft_strlen(g_token_types[token->type]),
			},
			(t_print_pattern){
				.pattern = "<tok>", .plen = 5,
				.replace = token->word.str - token->loffset,
				.rlen = token->word.len + token->loffset + token->roffset,
			},
			(t_print_pattern){
				.pattern = "<suggest>", .plen = 9,
				.replace = suggestion,
				.rlen = ft_strlen(suggestion),
			}));
}

static inline bool				_exec_parse_errors(
		t_parse_state_machine *m,
		t_token *token,
		char *file_path,
		char *file
)
{
	char						*s;

	if (!m->state.error)
	{
		m->state.errors += g_error_critical[m->next_state];
		m->state.warnings += !g_error_critical[m->next_state];
		print((t_print){
				.level = (g_error_critical[m->next_state]) ? LOG_CRIT : LOG_WARN,
				.printer = compiler_printer,
				.destructor = compiler_printer_free,
				.flags = (t_print_flags){
					.no_exit = true,
				},
				.data = &(t_print_compiler){
					.target = token,
					.message = _get_formated_error(m, token, file_path),
					.file_path = file_path,
					.file = file,
				},
		});
		if (g_error_critical[m->next_state])
			return ((m->state.error = true));
		return (false);
	}
	return (true);
}

static bool						g_token_blacklist[TOK_MAX] = {
	[TOK_NEWLINE] = true,
	[TOK_SEPARATOR] = true,
};

static bool						g_token_expr[TOK_MAX] = {
	[TOK_ANY_EXPR] = true,
	[TOK_DIRECT_NUMBER] = true,
	[TOK_NUMBER] = true,
	[TOK_INDIRECT_REF] = true,
	[TOK_DIRECT_REF] = true,
};

static inline bool				_exec_parse_tree_climb(
		t_ast_constructor_param param,
		t_parse_state_machine *m,
		t_ast_instruction *instruction,
		t_token **token
)
{
	/* print_token(*token); */
	if (m->next_state == PARSER_EXEC)
	{
		// Time to return the node to the ast as it is now valid
		/* print_ast_node_tree(&(t_ast_node){ */
		/*         .instruction = *instruction */
		/* }); */
		return (true);
	}
	else if (m->next_state == PARSER_SKIP)
	{
		instruction->len = 0;
		return (true);
	}
	else if (g_token_expr[(*token)->type])
	{
		/* printf("Expr in instruction\n"); */
		t_ast_construct		c = construct_expr((t_ast_constructor_param){
				.ast = param.ast,
				.binary = param.binary,
				.tokens = param.tokens,
				.input = (*token),
				.index = param.index,
		});
		m->state = (t_state){
			.error = c.state.error || m->state.error,
			.errors = c.state.errors + m->state.errors,
			.warnings = c.state.warnings + m->state.warnings,
		};
		*token = &instruction->args[instruction->len].expr.target;
		// Proxy any token error to the expression
		/* print_ast_node_tree(&(c.node)); */
		m->next_state = g_parse_tree[m->type][m->prev_state][c.node.expr.result_type];
		instruction->args[instruction->len++] = c.node;
		/* printf("%s > %s\n", g_token_types[(*token)->type], g_states[m->next_state]); */
	}
	else if (!g_token_blacklist[(*token)->type])
		instruction->args[instruction->len++] = (t_ast_node){
			.token = (t_ast_token){
					.type = AST_TYPE_TOKEN,
					.token = **token
			},
		};
	return (false);
}

t_ast_construct					construct_instruction(
		t_ast_constructor_param param
){
	t_parse_state_machine		m;
	t_ast_instruction			instruction;

	m = (t_parse_state_machine){
		.prev_state = PARSER_OP_START,
		.op_token = param.input,
		.type = _get_op(param.input),
		.file = param.binary->source_content,
	};
	instruction = (t_ast_instruction){
		.type = AST_TYPE_INSTRUCTION,
		.op = param.input,
		.args = malloc(sizeof(t_ast_node) * 4),
	};
	while ((m.token = ft_dynarray_iterate(param.tokens, param.index, sizeof(t_token))))
	{
		m.next_state = g_parse_tree[m.type][m.prev_state][m.token->type];
		if ((m.next_state > PARSER_MAX_ERR || !g_error_critical[m.next_state])
			&& _exec_parse_tree_climb(param, &m, &instruction, &m.token))
				break ;
		if ((m.next_state < PARSER_MAX_ERR || m.state.error)
				&& _exec_parse_errors(&m,
					(m.type == OP_NONE) ? m.op_token : m.token,
					param.binary->source_path, param.binary->source_content))
			break ;
		m.prev_state = m.next_state;
	}
	return ((t_ast_construct){
			.state = m.state,
			.valid = !m.state.error,
			.node = (t_ast_node){
				.instruction = instruction
			},
	});
}
