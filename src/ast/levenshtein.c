/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   levenshtein.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/07 07:32:36 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/07 13:32:16 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "compiler.h"
#include "libft.h"

static inline size_t	_min3(
		size_t a,
		size_t b,
		size_t c
)
{
	if (a < b)
		if (a < c)
			return (a);
		else
			return (c);
	else if (b < c)
		return (b);
	return (c);
}

/*
** Get the editing distance between words
*/

size_t					levenshtein(
		const char *a,
		const size_t alen,
		const char *b,
		const size_t blen
)
{
	size_t		i;
	size_t		j;
	size_t		cost;
	size_t		*cache;

	if (a == b || !(cache = malloc((alen + 1) * (blen + 1) * sizeof(size_t))))
		return (0);
	cache[0] = 0;
	i = 0;
	while (++i <= alen)
		cache[i] = i;
	i = 0;
	while (++i <= blen)
		cache[i * (alen + 1)] = i;
	i = 0;
	while (++i <= alen && !(j = 0))
		while (++j <= blen)
			cache[i + j * (alen + 1)] = _min3(
					cache[i - 1 + j * (alen + 1)] + 1,
					cache[i + (j - 1) * (alen + 1)] + 1,
					cache[i - 1 + (j - 1) * (alen + 1)]
						+ (a[i - 1] != b[j - 1]) * 2);
	cost = cache[alen + blen * (alen + 1)];
	free(cache);
	return (cost);
}

char					*get_suggestion(
		char *input,
		size_t len,
		t_string_list data
)
{
	char		*suggest;
	size_t		i;
	size_t		min;
	size_t		leven;

	suggest = NULL;
	min = SIZE_MAX;
	i = 0;
	while (i < data.len)
	{
		if (min > (leven = levenshtein(input, len,
						data.list[i].str, data.list[i].len)))
		{
			min = leven;
			suggest = data.list[i].str;
		}
		++i;
	}
	return (suggest);
}
