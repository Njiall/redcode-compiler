/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   construct_expr.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/19 08:38:53 by mbeilles          #+#    #+#             */
/*   Updated: 2019/11/02 19:55:43 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "ast.h"
#include "compiler.h"

/* typedef enum			 */

static inline bool		_is_op(
		t_token *token
)
{
	return (token->type >= TOK_OP_RNG_START && token->type <= TOK_OP_RNG_END
			&& token->type != TOK_EXPR_BRC_CLOSE
			&& token->type != TOK_EXPR_BRC_OPEN);
}

static inline bool		_is_param(
		t_token *token
)
{
	return (
			token->type == TOK_NUMBER
			|| token->type == TOK_DIRECT_NUMBER
			|| token->type == TOK_DIRECT_REF
			|| token->type == TOK_INDIRECT_REF
	);
}

static char						*g_expr_types[EXPR_TYPE_MAX] = {
	[EXPR_TYPE_OP] = "Operation",
	[EXPR_TYPE_EXPR] = "Sub expression",
	[EXPR_TYPE_NUMBER] = "Number",
	[EXPR_TYPE_LONG_NUMBER] = "Long number",
	[EXPR_TYPE_TOKEN] = "Token",
};

typedef struct			s_operator
{
	char				*str;
	t_expr_op_type		type;
	size_t				priority;
	size_t				arg_num;
	bool				assign_right;
}						t_operator;

static const t_operator	g_ops[TOK_MAX] = {
	[TOK_OP_ADD] = (t_operator){
		.str = "+",
		.type = EXPR_OP_ADD,
		.priority = 2,
		.arg_num = 2,
	},
	[TOK_OP_SUB] = (t_operator){
		.str = "-",
		.type = EXPR_OP_SUB,
		.priority = 2,
		.arg_num = 2,
	},
	[TOK_OP_DIV] = (t_operator){
		.str = "/",
		.type = EXPR_OP_DIV,
		.priority = 3,
		.arg_num = 2,
	},
	[TOK_OP_MULT] = (t_operator){
		.str = "*",
		.type = EXPR_OP_MULT,
		.priority = 3,
		.arg_num = 2,
	},
	[TOK_OP_AND_BIT] = (t_operator){
		.str = "&",
		.type = EXPR_OP_AND_BIT,
		.priority = 8,
		.arg_num = 2,
	},
	[TOK_OP_XOR_BIT] = (t_operator){
		.str = "^",
		.type = EXPR_OP_XOR_BIT,
		.priority = 9,
		.arg_num = 2,
	},
	[TOK_OP_OR_BIT] = (t_operator){
		.str = "|",
		.type = EXPR_OP_OR_BIT,
		.priority = 10,
		.arg_num = 2,
	},
	[TOK_OP_AND_LOG] = (t_operator){
		.str = "&&",
		.type = EXPR_OP_AND_LOG,
		.priority = 11,
		.arg_num = 2,
	},
	[TOK_OP_XOR_LOG] = (t_operator){
		.str = "^^",
		.type = EXPR_OP_XOR_LOG,
		.priority = 12,
		.arg_num = 2,
	},
	[TOK_OP_OR_LOG] = (t_operator){
		.str = "||",
		.type = EXPR_OP_OR_LOG,
		.priority = 13,
		.arg_num = 2,
	},
	[TOK_OP_LSHIFT] = (t_operator){
		.str = "<<",
		.type = EXPR_OP_LSHIFT,
		.priority = 5,
		.arg_num = 2,
	},
	[TOK_OP_RSHIFT] = (t_operator){
		.str = ">>",
		.type = EXPR_OP_RSHIFT,
		.priority = 5,
		.arg_num = 2,
	},
	[TOK_OP_NOT] = (t_operator){
		.str = "!",
		.type = EXPR_OP_NOT,
		.priority = 1,
		.arg_num = 1,
		.assign_right = true,
	},
};

typedef enum				e_expr_state
{
	ST_EXPR_ERR_DEFAULT,

	ST_EXPR_ERR_NO_ARGS,
	ST_EXPR_ERR_NO_OP,
	ST_EXPR_ERR_IMPL_CAST,
	ST_EXPR_ERR_NO_CLOSE_BRACE,
	ST_EXPR_ERR_NO_OPEN_BRACE,
	ST_EXPR_ERR_EMPTY,

	ST_EXPR_MAX,
}							t_expr_state;

static bool						g_expr_critical[ST_EXPR_MAX] = {
	[ST_EXPR_ERR_DEFAULT] = true,
	[ST_EXPR_ERR_NO_ARGS] = true,
	[ST_EXPR_ERR_NO_OP] = true,
	[ST_EXPR_ERR_NO_OPEN_BRACE] = true,
	[ST_EXPR_ERR_NO_CLOSE_BRACE] = true,
};

static char						*g_expr_errors[ST_EXPR_MAX] = {
	[ST_EXPR_ERR_IMPL_CAST] = "implicit expanded casting to <type>. \e[1;30m[\e[0;35m-Wauto-cast\e[0;1;30m]\e[0m",
	[ST_EXPR_ERR_DEFAULT] = "invalid expression.",
	[ST_EXPR_ERR_NO_OP] = "too much arguments for <op> operator.",
	[ST_EXPR_ERR_NO_ARGS] = "not enough arguments for <op> operator.",
	[ST_EXPR_ERR_NO_OPEN_BRACE] = "no matching opening brace.",
	[ST_EXPR_ERR_NO_CLOSE_BRACE] = "no matching closing brace.",
};

static char						*g_token_types[TOK_MAX] = {
	[TOK_NONE] = 			"none",
	[TOK_WORD] = 			"\e[1;30;4mword\e[0m",
	[TOK_NUMBER] = 			"\e[1;30;4mnumber\e[0m",
	[TOK_DIRECT_NUMBER] = 	"\e[1;30;4mdirect number\e[0m",
	[TOK_NEWLINE] = 		"\e[1;30;4mnewline\e[0m",
	[TOK_SEPARATOR] = 		"\e[1;30;4mseparator\e[0m",
	[TOK_INDIRECT_REF] = 	"\e[1;30;4mindirect reference\e[0m",
	[TOK_DIRECT_REF] = 		"\e[1;30;4mdirect reference\e[0m",
	[TOK_KEY_REF] = 		"\e[1;30;4mkey\e[0m",
	[TOK_PROP] = 			"\e[1;30;4mproperty\e[0m",
	[TOK_STRING] = 			"\e[1;30;4mstring\e[0m",
	[TOK_REGISTER] = 		"\e[1;30;4mregister\e[0m",
	[TOK_OP_ADD] = 			"\e[1;30;4madd\e[0m",
	[TOK_OP_SUB] = 			"\e[1;30;4msub\e[0m",
	[TOK_OP_DIV] = 			"\e[1;30;4mdivide\e[0m",
	[TOK_OP_MULT] = 		"\e[1;30;4mmult\e[0m",
	[TOK_EXPR_BRC_CLOSE] = 	"\e[1;30;4m)\e[0m",
	[TOK_EXPR_BRC_OPEN] = 	"\e[1;30;4m(\e[0m",
};

# define TOK_STR_DIRECT			"a \e[1;30;4mdirect value\e[0m"

static inline void		_print_error(
	t_ast_constructor_param param,
	t_expr_state state,
	t_token *target,
	char *suggest
)
{
	char				*msg = replace_format_placeholders(
			g_expr_errors[state],
			2,
			(t_print_pattern){
				.pattern = "<op>", .plen = 4,
				.replace = target->str - target->loffset,
				.rlen = target->len + target->loffset + target->roffset,
			},
			(t_print_pattern){
				.pattern = "<type>", .plen = 6,
				.replace = TOK_STR_DIRECT,
				.rlen = ft_strlen(TOK_STR_DIRECT),
			}
	);
	print((t_print){
			.level = (g_expr_critical[state]) ? LOG_CRIT : LOG_WARN,
			.printer = compiler_printer,
			.destructor = compiler_printer_free,
			.flags = (t_print_flags){
				.no_exit = true,
			},
			.data = &(t_print_compiler){
				.suggest = suggest,
				.suggest_offset = (state == ST_EXPR_ERR_IMPL_CAST) ? -1 : 0,
				.target = target,
				.message = msg,
				.file_path = param.binary->source_path,
				.file = param.binary->source_content,
			},
	});
}

static size_t			_convert_ast_expr_type(
		t_ast_constructor_param *param,
		t_token_type good,
		t_expr *expr
)
{
	size_t				converted = 0;

	for (size_t i = 0; i < expr->len; i++)
	{
		if (good != expr->args[i].result_type)
		{
			if (!_is_param(&expr->args[i].token)) {
				for (size_t j = 0; j < expr->len; ++j) {
					converted += _convert_ast_expr_type(param, good, expr->args + j);
				}
			}
			else
			{
				char *tmp = ft_strndup(expr->args[i].token.str - expr->args[i].token.loffset,
						expr->args[i].token.len + expr->args[i].token.roffset + expr->args[i].token.loffset);

				_print_error(*param, ST_EXPR_ERR_IMPL_CAST, &expr->args[i].token, ft_strajoin(2, "%", tmp));
				expr->args[i].result_type = good;
				converted++;
				free(tmp);
			}
		}
	}
	return (converted);
}

static inline t_state	_create_ast_expr(
		t_ast_constructor_param param,
		t_dynarray *expr,
		t_dynarray *ops,
		t_token *op
)
{
	t_token				operator;

	// Create expr node
	// Pop operator out of stack
	if (!ft_dynarray_pop(ops, (void*)&operator, sizeof(t_token))) {
		_print_error(param, ST_EXPR_ERR_NO_OP, op, NULL);
		return ((t_state){.error = 1, .errors = 1});
	}
	// Create template
	t_expr			result = (t_expr){
		.op = (t_expr_op){
			.type = EXPR_TYPE_OP,
			.op = g_ops[op->type].type,
			.token = *op,
			.result_type = TOK_NUMBER,
			.args = malloc(g_ops[op->type].arg_num * sizeof(t_expr)),
		},
	};
	// Pop operands and construct new sub expr
	for (; result.len < g_ops[operator.type].arg_num; result.len++)
		if (!ft_dynarray_pop(expr, (void*)(result.args + g_ops[op->type].arg_num
						- result.len - 1), sizeof(t_expr)))
		{
			_print_error(param, ST_EXPR_ERR_NO_ARGS, op, NULL);
			return ((t_state){.error = 1, .errors = 1});
		}
	// If no args
	if (result.len)
	{
		/* result.result_type = result.args[0].result_type; */
		t_token_type		type = TOK_NUMBER;
		for (size_t i = 0; i < result.len; i++)
			if (type != result.args[i].result_type && type == TOK_NUMBER)
				type = TOK_DIRECT_NUMBER;
		result.result_type = type;

		size_t i;
		if ((i = _convert_ast_expr_type(&param, type, &result)))
		{
			ft_dynarray_push(expr, &result, sizeof(t_expr));
			return ((t_state){.warnings = i});
		}
	}
	// Push new expr
	ft_dynarray_push(expr, &result, sizeof(t_expr));
	return ((t_state){});
}

static inline t_state	_merge_states(
	t_state s1,
	t_state s2,
	t_token *target
)
{
	return ((t_state){
		.error = s1.error || s2.error,
		.errors = s1.errors + s2.errors,
		.warnings = s1.warnings + s2.warnings,
		.line = target->line,
		.col = target->col,
	});
}

t_ast_construct			construct_expr(
		t_ast_constructor_param param
)
{
	t_state				state;
	t_dynarray			expr;
	t_dynarray			operators;
	t_token				*token;
	t_token				*op;
	t_token				tmp;
	t_expr				operator;
	uint64_t			i;

	state = (t_state){};

	expr = ft_dynarray_create_loc(0, 0); // Queue
	operators = ft_dynarray_create_loc(0, 0); // Stack
	// Offset to include input token. Which is most likely a number.
	(*param.index)--;
	while (!state.error && (token = ft_dynarray_iterate(param.tokens, param.index, sizeof(t_token))))
	{
		/* printf("token: "); */
		/* print_token(token); */

		if (_is_param(token)) {

			// Handle numbers / references
			ft_dynarray_push(&expr, &(t_expr){
					.type = EXPR_TYPE_NUMBER,
					.token = *token,
					.result_type = token->type,
			}, sizeof(t_expr));

		} else if (_is_op(token)) {

			// Handle operators / functions
			// To handle n args operators, pop n tokens from the expression stack
			// and if any one of those isn't a result expression or there isn't
			// enough the operation is invalid.
			while (!state.error
					&& (op = (t_token*)ft_dynarray_stack_top(&operators, sizeof(t_token)))
					&& (g_ops[token->type].priority > g_ops[op->type].priority
						|| (g_ops[token->type].priority == g_ops[op->type].priority && !g_ops[token->type].assign_right))
					&& op->type != TOK_EXPR_BRC_OPEN
			)
			{
				state = _merge_states(_create_ast_expr(param, &expr, &operators, op), state, token);
			}
			ft_dynarray_push(&operators, token, sizeof(t_token));

		} else if (token->type == TOK_EXPR_BRC_OPEN) {

			// Handle opening bracket
			ft_dynarray_push(&operators, token, sizeof(t_token));

		} else if (token->type == TOK_EXPR_BRC_CLOSE) {

			/* printf("token: "); */
			/* print_token(token); */

			// Handle closing bracket
			t_token_type type = TOK_NONE;
			while (!state.error
					&& (op = ft_dynarray_stack_top(&operators, sizeof(t_token)))
					&& (type = op->type) != TOK_EXPR_BRC_OPEN)
				state = _merge_states(_create_ast_expr(param, &expr, &operators, op), state, op);
			if (type != TOK_EXPR_BRC_OPEN)
			{
				_print_error(param, ST_EXPR_ERR_NO_OPEN_BRACE, token, ft_strdup("()"));
				state.error = true;
				state.errors++;
			}
			// Pops open bracket into the void
			ft_dynarray_pop(&operators, &tmp, sizeof(t_token));

		} else {
			// Out of an expression
			(*param.index)--;
			break;
		}
	}
	while (!state.error && (token = ft_dynarray_stack_top(&operators, sizeof(t_token))))
	{
		// Since scope
		if (token->type == TOK_EXPR_BRC_OPEN)
		{
			_print_error(param, ST_EXPR_ERR_NO_CLOSE_BRACE, token, ft_strdup("()"));
			state.error = true;
			state.errors++;
		}
		state = _merge_states(_create_ast_expr(param, &expr, &operators, token), state, token);
	}

	// Handles Errors

	if (!state.error && expr.index > sizeof(t_expr))
	{
		op = ft_dynarray_get(param.tokens, *param.index, sizeof(t_token));
		_print_error(param, ST_EXPR_ERR_DEFAULT, &(t_token){
						.line = param.input->line,
						.col = param.input->col,
						.len = op->col - param.input->col,
						.str = param.input->str,
					}, NULL);
		state.error = true;
		state.errors++;
	}

	// TODO Handle invalid expression errors
	// If expression stack doesn't contain 1 element,
	// then the expression is invalid

	if (expr.index)
		operator = *((t_expr*)ft_dynarray_stack_top(&expr, sizeof(t_expr)));
	else
		operator = (t_expr){};
	op = ft_dynarray_get(param.tokens, *param.index, sizeof(t_token));

	/* ft_dynarray_destroy(&expr, false); */
	/* ft_dynarray_destroy(&operators, false); */

	return ((t_ast_construct){
			.valid = !state.error,
			.state = state,
			.node = (t_ast_node){
				.expr = (t_ast_expr){
					.type = AST_TYPE_EXPR,
					.expr = operator,
					.result_type = operator.result_type,
					.target = (t_token){
						.number = (t_token_num){
							.type = operator.result_type,
							.line = param.input->line,
							.col = param.input->col,
							.len = op->col - param.input->col,
							.loffset = param.input->loffset,
							.roffset = op->roffset,
							.str = param.input->str,
						},
					},
				},
			}
	});
}
