/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   construct_property.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/25 17:16:52 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/30 12:31:17 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdbool.h>
#include "compiler.h"
#include "ast.h"
#include "libft.h"

static inline t_ast_construct	construct_prop_name(
		t_token *name,
		t_token *value,
		char *file,
		char *file_path
)
{
	if (value->word.len > 128)
	{
		print((t_print){
				.level = LOG_WARN,
				.printer = compiler_printer,
				.data = &(t_print_compiler){
					.target = value,
					.file_path = file_path,
					.file = file,
					.message = "Name too long. Will cut at 128 chars.",
				},
		});
		return ((t_ast_construct){.state = (t_state){.warnings = 1}});
	}
	return ((t_ast_construct){});
}

static inline t_ast_construct	construct_prop_comment(
		t_token *name,
		t_token *value,
		char *file,
		char *file_path
)
{
	if (value->word.len > 2048)
	{
		print((t_print){
				.level = LOG_WARN,
				.printer = compiler_printer,
				.data = &(t_print_compiler){
					.target = value,
					.file_path = file_path,
					.file = file,
					.message = "Comment too long. Will cut at 2048 chars.",
				},
		});
		return ((t_ast_construct){.state = (t_state){.warnings = 1}});
	}
	return ((t_ast_construct){});
}

typedef enum		e_property {
	PROP_UNKNOWN,
	PROP_NAME,
	PROP_COMMENT,
	PROP_MAX
}					t_property;

static t_ast_construct	(*g_prop_subparser[PROP_MAX])(t_token*, t_token*, char*, char*) = {
	[PROP_NAME] = 		&construct_prop_name,
	[PROP_COMMENT] = 	&construct_prop_comment,
};

static char				*g_prop_keyword[PROP_MAX] = {
	[PROP_NAME] = 		"name",
	[PROP_COMMENT] = 	"comment",
};

static t_token_type		g_prop_type[PROP_MAX] = {
	[PROP_NAME] = 		TOK_STRING,
	[PROP_COMMENT] = 	TOK_STRING,
};

static inline t_ast_construct	_print_severe(
		t_token *target,
		char *file,
		char *file_path,
		char *name,
		char *message
)
{
	print((t_print){
			.level = LOG_CRIT, .printer = compiler_printer,
			.destructor = compiler_printer_free,
			.flags = (t_print_flags){.no_exit = true,},
			.data = &(t_print_compiler){
				.target = target, .file_path = file_path, .file = file,
				.message = ft_strajoin(3, message, name, "."),
			},
	});
	free(name);
	return ((t_ast_construct){.state = (t_state){.error = true, .errors = 1}});
}

t_ast_construct					construct_property(
		t_ast_constructor_param param
)
{
	t_state			state;
	t_token			*value;
	char			*name;
	int				i;

	state = (t_state){};
	name = ft_strndup(param.input->word.str, param.input->len);
	i = PROP_UNKNOWN;
	while (++i < PROP_MAX)
		if (param.input->word.len == ft_strlen(g_prop_keyword[i]) &&
				!ft_strncmp(param.input->word.str, g_prop_keyword[i], param.input->len))
		{
			if ((value = ft_dynarray_get(param.tokens, *param.index, sizeof(t_token)))
					&& value->type != TOK_NEWLINE && ++(*param.index))
				if (g_prop_type[i] == value->type)
					return (g_prop_subparser[i](param.input, value, param.binary->source_content, param.binary->source_path));
				else
					return (_print_severe(value, param.binary->source_content, param.binary->source_path, name,
							"Invalid argument type for "));
			else
				return (_print_severe((value) ? value : param.input, param.binary->source_content, param.binary->source_path, name,
							"No argument given to "));
		}
	return (_print_severe(param.input, param.binary->source_content, param.binary->source_path, name,
				"No property named "));
}
