/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tokenizer.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/28 14:29:01 by mbeilles          #+#    #+#             */
/*   Updated: 2019/11/02 19:51:18 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include <stdio.h>

#include "../../include/compiler.h"
#include "../../libft/include/dynarray.h"
#include "../../libft/include/libft.h"

char				*g_word_str[ST_MAX] = {
	[ST_INIT] = 			"init  ",
	[ST_ERROR] = 			"error ",
	[ST_WORD] = 			"word  ",
	[ST_NUMBER] = 			"num   ",
	[ST_DIRECT_NUMBER] = 	"dir nm",
	[ST_SPACE] = 			"sp    ",
	[ST_NEWLINE] = 			"\\n    ",
	[ST_SEPARATOR] = 		",     ",
	[ST_EOF] = 				"eof   ",
	[ST_LABEL_KEY] = 		"key   ",
	[ST_INDIRECT_LABEL] = 	"indirect label",
	[ST_DIRECT_LABEL] = 	"direct label",
	[ST_DIRECT] = 			"direct",
	[ST_COMMENT] = 			"comment",
	[ST_PROP] = 			"prop",
	[ST_PROP_NAME] = 		"prop name",
	[ST_STR_D] = 			"string \"",
	[ST_STR_S] = 			"string '",

	[ST_PLUS] = 			"+       ",
	[ST_MINUS] = 			"-       ",
	[ST_MULT] = 			"*       ",
	[ST_DIVIDE] = 			"/       ",

	[ST_AND_LOG] = 			"&&      ",
	[ST_XOR_LOG] = 			"^^      ",
	[ST_OR_LOG] = 			"||      ",
	[ST_AND_BIT] = 			"&       ",
	[ST_XOR_BIT] = 			"^       ",
	[ST_OR_BIT] = 			"|       ",

	[ST_LSHIFT] = 			"<<      ",
	[ST_RSHIFT] = 			">>      ",

	[ST_LCHEV] = 			"<       ",
	[ST_RCHEV] = 			">       ",

	[ST_NOT] = 				"!       ",

	[ST_BRACE_OPEN] = 		"(       ",
	[ST_BRACE_CLOSE] = 		")       ",
};

static char						*g_token_types[TOK_MAX] = {
	[TOK_NONE] = 			"none",
	[TOK_WORD] = 			TOK_STR_WORD,
	[TOK_NUMBER] = 			TOK_STR_NUMBER,
	[TOK_DIRECT_NUMBER] = 	TOK_STR_DIRECT_NUMBER,
	[TOK_NEWLINE] = 		TOK_STR_NEWLINE,
	[TOK_SEPARATOR] = 		TOK_STR_SEPARATOR,
	[TOK_INDIRECT_REF] = 	TOK_STR_INDIRECT_REF,
	[TOK_DIRECT_REF] = 		TOK_STR_DIRECT_REF,
	[TOK_KEY_REF] = 		TOK_STR_KEY_REF,
	[TOK_PROP] = 			TOK_STR_PROP,
	[TOK_STRING] = 			TOK_STR_STRING,
	[TOK_REGISTER] = 		TOK_STR_REGISTER,
	[TOK_OP_ADD] = 			"\e[1;32madd\e[0m",
	[TOK_OP_SUB] = 			"\e[1;32msub\e[0m",
	[TOK_OP_DIV] = 			"\e[1;32mdivide\e[0m",
	[TOK_OP_MULT] = 		"\e[1;32mmult\e[0m",
	[TOK_EXPR_BRC_CLOSE] = 	"\e[1;32m)\e[0m",
	[TOK_EXPR_BRC_OPEN] = 	"\e[1;32m(\e[0m",
};

char				*get_printable(char c) {
	static char		s[4] = "' '";
	switch (c) {
		case '\t':
			return ("'\\t'");
		case '\n':
			return ("'\\n'");
		case '\v':
			return ("'\\v'");
		case '\r':
			return ("'\\r'");
		case '\0':
			return ("eof");
		default:
			s[1] = c;
			return (s);
	}
}

t_char_class		get_char_class[256] = {
	[' '] = CH_SPACE,
	['\t'] = CH_SPACE,
	['\v'] = CH_SPACE,
	['\f'] = CH_SPACE,
	['\r'] = CH_SPACE,

	[','] = CH_SEPARATOR,
	[':'] = CH_LABEL,
	['%'] = CH_DIRECT,
	['#'] = CH_COMMENT,
	[';'] = CH_COMMENT,

	['a' ... 'z'] = CH_WORD,
	['A' ... 'Z'] = CH_WORD,
	['_'] = CH_WORD,

	['0' ... '9'] = CH_NUMBER,

	['+'] = CH_PLUS,
	['-'] = CH_MINUS,
	['/'] = CH_SLASH,
	['*'] = CH_STAR,

	['&'] = CH_AMP,
	['^'] = CH_CARET,
	['|'] = CH_PIPE,

	['<'] = CH_LCHEV,
	['>'] = CH_RCHEV,

	['!'] = CH_BANG,
	['('] = CH_BRACE_OPEN,
	[')'] = CH_BRACE_CLOSE,

	['\n'] = CH_NEWLINE,

	['\0'] = CH_EOF,

	['.'] = CH_DOT,
	['"'] = CH_DQUOTE,
	['\''] = CH_QUOTE,
};

t_word_state		g_word_transition[ST_MAX][CH_MAX] = {
	/*
	** ========================================================================
	** 			Word syntax
	** ========================================================================
	*/
	[ST_INIT][CH_WORD] = ST_WORD,
	[ST_NEWLINE][CH_WORD] = ST_WORD,
	[ST_SEPARATOR][CH_WORD] = ST_WORD,
	[ST_SPACE][CH_WORD] = ST_WORD,
	[ST_OPERATOR][CH_WORD] = ST_WORD,

	[ST_WORD][CH_WORD] = ST_WORD,
	[ST_WORD][CH_NUMBER] = ST_WORD,
	[ST_NUMBER][CH_WORD] = ST_WORD,
	/*
	** ========================================================================
	** 			Operators syntax
	** ========================================================================
	*/
	// '+'
	[ST_INIT ... ST_DIRECT - 1][CH_PLUS] = 				ST_PLUS,
	[ST_DIRECT + 1 ... ST_INVALID_END - 1][CH_PLUS] = 	ST_PLUS,
	// '-'
	[ST_INIT ... ST_DIRECT - 1][CH_MINUS] = 			ST_MINUS,
	[ST_DIRECT + 1 ... ST_INVALID_END - 1][CH_MINUS] = 	ST_MINUS,
	// '*'
	[ST_INCONDITIONAL][CH_STAR] = 						ST_MULT,
	// '/'
	[ST_INCONDITIONAL][CH_SLASH] = 						ST_DIVIDE,
	// '&'
	[ST_INIT ... ST_AND_BIT - 1][CH_AMP] = 				ST_AND_BIT,
	[ST_AND_BIT + 1 ... ST_INVALID_END - 1][CH_AMP] = 	ST_AND_BIT,
	// '&&'
	[ST_AND_BIT][CH_AMP] = 								ST_AND_LOG,
	// '^'
	[ST_INIT ... ST_XOR_BIT - 1][CH_CARET] = 			ST_XOR_BIT,
	[ST_XOR_BIT + 1 ... ST_INVALID_END - 1][CH_CARET] = ST_XOR_BIT,
	// '^^'
	[ST_XOR_BIT][CH_CARET] = 							ST_XOR_LOG,
	// '|'
	[ST_INIT ... ST_OR_BIT - 1][CH_PIPE] = 				ST_OR_BIT,
	[ST_OR_BIT + 1 ... ST_INVALID_END - 1][CH_PIPE] = 	ST_OR_BIT,
	// '||'
	[ST_OR_BIT][CH_PIPE] = 								ST_OR_LOG,
	// '<<'
	[ST_INIT ... ST_LCHEV - 1][CH_LCHEV] = 				ST_LCHEV,
	[ST_LCHEV + 1 ... ST_INVALID_END - 1][CH_LCHEV] = 	ST_LCHEV,
	[ST_LCHEV][CH_LCHEV] = 								ST_LSHIFT,
	// '>>'
	[ST_INIT ... ST_RCHEV - 1][CH_RCHEV] = 				ST_RCHEV,
	[ST_RCHEV + 1 ... ST_INVALID_END - 1][CH_RCHEV] = 	ST_RCHEV,
	[ST_RCHEV][CH_RCHEV] = 								ST_RSHIFT,
	// '!'
	[ST_INCONDITIONAL][CH_BANG] = 		ST_NOT,
	// '('
	[ST_INCONDITIONAL][CH_BRACE_OPEN] = 	ST_BRACE_OPEN,
	// ')'
	[ST_INCONDITIONAL][CH_BRACE_CLOSE] = 	ST_BRACE_CLOSE,

	/*
	** ========================================================================
	** 			Number syntax
	** ========================================================================
	*/
	[ST_INIT][CH_NUMBER] = ST_NUMBER,
	[ST_OPERATOR][CH_NUMBER] = ST_NUMBER,
	[ST_SEPARATOR][CH_NUMBER] = ST_NUMBER,
	[ST_SPACE][CH_NUMBER] = ST_NUMBER,
	[ST_NEWLINE][CH_NUMBER] = ST_NUMBER,
	[ST_NUMBER][CH_NUMBER] = ST_NUMBER,
	/*
	** ========================================================================
	** 			Direct syntax
	** ========================================================================
	*/
	[ST_INIT][CH_DIRECT] = ST_DIRECT,
	[ST_OPERATOR][CH_DIRECT] = ST_DIRECT,
	[ST_SEPARATOR][CH_DIRECT] = ST_DIRECT,
	[ST_SPACE][CH_DIRECT] = ST_DIRECT,
	[ST_NEWLINE][CH_DIRECT] = ST_DIRECT,
	/*
	** ========================================================================
	** 			Direct number syntax
	** ========================================================================
	*/
	[ST_DIRECT][CH_PLUS ... CH_MINUS] = ST_DIRECT_NUMBER,
	[ST_DIRECT][CH_NUMBER] = ST_DIRECT_NUMBER,
	[ST_DIRECT_NUMBER][CH_NUMBER] = ST_DIRECT_NUMBER,
	/*
	** ========================================================================
	** 			Space syntax
	** ========================================================================
	*/
	[ST_INIT][CH_SPACE] = ST_SPACE,
	[ST_SPACE][CH_SPACE] = ST_SPACE,
	[ST_NEWLINE][CH_SPACE] = ST_SPACE,
	[ST_SEPARATOR][CH_SPACE] = ST_SPACE,

	[ST_WORD][CH_SPACE] = ST_SPACE,
	[ST_NUMBER][CH_SPACE] = ST_SPACE,
	[ST_DIRECT_NUMBER][CH_SPACE] = ST_SPACE,
	[ST_INDIRECT_LABEL][CH_SPACE] = ST_SPACE,
	[ST_DIRECT_LABEL][CH_SPACE] = ST_SPACE,
	[ST_LABEL_KEY][CH_SPACE] = ST_SPACE,

	[ST_OPERATOR][CH_SPACE] = ST_SPACE,

	[ST_EOF][CH_SPACE] = ST_SPACE,
	/*
	** ========================================================================
	** 			Separator syntax
	** ========================================================================
	*/
	[ST_INIT ... ST_INVALID_END - 1][CH_SEPARATOR] = ST_SEPARATOR,
	/*
	** ========================================================================
	** 			Newline syntax
	** ========================================================================
	*/
	[ST_INIT ... ST_INVALID_END - 1][CH_NEWLINE] = ST_NEWLINE,
	/*
	** ========================================================================
	** 			Comment syntax
	** ========================================================================
	*/
	[ST_INIT][CH_COMMENT] = 			ST_COMMENT,
	[ST_WORD][CH_COMMENT] = 			ST_COMMENT,
	[ST_NUMBER][CH_COMMENT] = 			ST_COMMENT,
	[ST_NEWLINE][CH_COMMENT] = 			ST_COMMENT,
	[ST_SPACE][CH_COMMENT] = 			ST_COMMENT,
	[ST_SEPARATOR][CH_COMMENT] = 		ST_COMMENT,
	[ST_LABEL_KEY][CH_COMMENT] = 		ST_COMMENT,
	[ST_DIRECT_LABEL][CH_COMMENT] = 	ST_COMMENT,
	[ST_INDIRECT_LABEL][CH_COMMENT] = 	ST_COMMENT,

	[ST_COMMENT][CH_NONE] = 			ST_COMMENT,
	[ST_COMMENT][CH_SEPARATOR] = 		ST_COMMENT,
	[ST_COMMENT][CH_SPACE] = 			ST_COMMENT,
	[ST_COMMENT][CH_WORD] = 			ST_COMMENT,
	[ST_COMMENT][CH_NUMBER] = 			ST_COMMENT,
	[ST_COMMENT][CH_LABEL] = 			ST_COMMENT,
	[ST_COMMENT][CH_DIRECT] = 			ST_COMMENT,
	[ST_COMMENT][CH_COMMENT] = 			ST_COMMENT,
	[ST_COMMENT][CH_DOT] = 				ST_COMMENT,
	[ST_COMMENT][CH_QUOTE] = 			ST_COMMENT,
	[ST_COMMENT][CH_DQUOTE] = 			ST_COMMENT,

	[ST_COMMENT][CH_PLUS] = 			ST_COMMENT,
	[ST_COMMENT][CH_STAR] = 			ST_COMMENT,
	[ST_COMMENT][CH_MINUS] = 			ST_COMMENT,
	[ST_COMMENT][CH_SLASH] = 			ST_COMMENT,
	[ST_COMMENT][CH_PIPE] = 			ST_COMMENT,
	[ST_COMMENT][CH_CARET] = 			ST_COMMENT,
	[ST_COMMENT][CH_AMP] = 				ST_COMMENT,
	[ST_COMMENT][CH_RCHEV] = 			ST_COMMENT,
	[ST_COMMENT][CH_LCHEV] = 			ST_COMMENT,
	[ST_COMMENT][CH_BANG] = 			ST_COMMENT,
	[ST_COMMENT][CH_BRACE_OPEN] = 		ST_COMMENT,
	[ST_COMMENT][CH_BRACE_CLOSE] = 		ST_COMMENT,

	[ST_COMMENT][CH_NEWLINE] = 			ST_NEWLINE,
	/*
	** ========================================================================
	** 			Label syntax
	** ========================================================================
	** Direct
	*/
	[ST_WORD][CH_LABEL] = 				ST_LABEL_KEY,
	[ST_NUMBER][CH_LABEL] = 			ST_LABEL_KEY,
	[ST_DIRECT][CH_LABEL] = 			ST_DIRECT_LABEL,
	[ST_DIRECT_LABEL][CH_WORD] = 		ST_DIRECT_LABEL,
	[ST_DIRECT_LABEL][CH_NUMBER] = 		ST_DIRECT_LABEL,
	/*
	** Indirect
	*/
	[ST_INIT][CH_LABEL] = 				ST_INDIRECT_LABEL,
	[ST_SPACE][CH_LABEL] = 				ST_INDIRECT_LABEL,
	[ST_SEPARATOR][CH_LABEL] = 			ST_INDIRECT_LABEL,
	[ST_PLUS][CH_LABEL] = 				ST_INDIRECT_LABEL,
	[ST_MINUS][CH_LABEL] = 				ST_INDIRECT_LABEL,
	[ST_MULT][CH_LABEL] = 				ST_INDIRECT_LABEL,
	[ST_DIVIDE][CH_LABEL] = 			ST_INDIRECT_LABEL,
	[ST_BRACE_OPEN][CH_LABEL] = 		ST_INDIRECT_LABEL,
	[ST_BRACE_CLOSE][CH_LABEL] = 		ST_INDIRECT_LABEL,

	[ST_INDIRECT_LABEL][CH_WORD] = 		ST_INDIRECT_LABEL,
	[ST_INDIRECT_LABEL][CH_NUMBER] = 	ST_INDIRECT_LABEL,
	/*
	** ========================================================================
	** 			Name syntax
	** ========================================================================
	*/
	[ST_INIT][CH_DOT] = 				ST_PROP,
	[ST_NEWLINE][CH_DOT] = 				ST_PROP,
	[ST_SPACE][CH_DOT] = 				ST_PROP,
	[ST_PROP][CH_SPACE] = 				ST_ERROR,

	[ST_PROP][CH_WORD] = 				ST_PROP_NAME,
	[ST_PROP_NAME][CH_WORD] = 			ST_PROP_NAME,
	[ST_PROP_NAME][CH_SPACE] = 			ST_SPACE,
	/*
	** ========================================================================
	** 			String syntax
	** ========================================================================
	*/
	[ST_INIT][CH_QUOTE] = 				ST_STR_S,
	[ST_WORD][CH_QUOTE] = 				ST_STR_S,
	[ST_OPERATOR][CH_QUOTE] = 			ST_STR_S,
	[ST_NUMBER][CH_QUOTE] = 			ST_STR_S,
	[ST_DIRECT_NUMBER][CH_QUOTE] = 		ST_STR_S,
	[ST_INDIRECT_LABEL][CH_QUOTE] = 	ST_STR_S,
	[ST_DIRECT_LABEL][CH_QUOTE] = 		ST_STR_S,
	[ST_LABEL_KEY][CH_QUOTE] = 			ST_STR_S,
	[ST_SPACE][CH_QUOTE] = 				ST_STR_S,
	[ST_NEWLINE][CH_QUOTE] = 			ST_STR_S,
	[ST_SEPARATOR][CH_QUOTE] = 			ST_STR_S,
	[ST_DIRECT][CH_QUOTE] = 			ST_STR_S,
	[ST_PROP_NAME][CH_QUOTE] = 			ST_STR_S,

	[ST_INIT][CH_DQUOTE] = 				ST_STR_D,
	[ST_WORD][CH_DQUOTE] = 				ST_STR_D,
	[ST_OPERATOR][CH_DQUOTE] = 			ST_STR_D,
	[ST_NUMBER][CH_DQUOTE] = 			ST_STR_D,
	[ST_DIRECT_NUMBER][CH_DQUOTE] = 	ST_STR_D,
	[ST_INDIRECT_LABEL][CH_DQUOTE] = 	ST_STR_D,
	[ST_DIRECT_LABEL][CH_DQUOTE] = 		ST_STR_D,
	[ST_LABEL_KEY][CH_DQUOTE] = 		ST_STR_D,
	[ST_SPACE][CH_DQUOTE] = 			ST_STR_D,
	[ST_NEWLINE][CH_DQUOTE] = 			ST_STR_D,
	[ST_SEPARATOR][CH_DQUOTE] = 		ST_STR_D,
	[ST_DIRECT][CH_DQUOTE] = 			ST_STR_D,
	[ST_PROP_NAME][CH_DQUOTE] = 		ST_STR_D,

	[ST_STR_S][CH_NONE] = 		ST_STR_S,
	[ST_STR_S][CH_WORD] = 		ST_STR_S,
	[ST_STR_S][CH_SEPARATOR] = 	ST_STR_S,
	[ST_STR_S][CH_SPACE] = 		ST_STR_S,
	[ST_STR_S][CH_NEWLINE] = 	ST_STR_S,
	[ST_STR_S][CH_LABEL] = 		ST_STR_S,
	[ST_STR_S][CH_DIRECT] = 	ST_STR_S,
	[ST_STR_S][CH_COMMENT] = 	ST_STR_S,
	[ST_STR_S][CH_PLUS] = 		ST_STR_S,
	[ST_STR_S][CH_NUMBER] = 	ST_STR_S,
	[ST_STR_S][CH_MINUS] = 		ST_STR_S,
	[ST_STR_S][CH_DOT] = 		ST_STR_S,
	[ST_STR_S][CH_DQUOTE] = 	ST_STR_S,

	[ST_STR_S][CH_QUOTE] = 		ST_SPACE,

	[ST_STR_D][CH_NONE] = 		ST_STR_D,
	[ST_STR_D][CH_WORD] = 		ST_STR_D,
	[ST_STR_D][CH_SEPARATOR] = 	ST_STR_D,
	[ST_STR_D][CH_SPACE] = 		ST_STR_D,
	[ST_STR_D][CH_NEWLINE] = 	ST_STR_D,
	[ST_STR_D][CH_NUMBER] = 	ST_STR_D,
	[ST_STR_D][CH_LABEL] = 		ST_STR_D,
	[ST_STR_D][CH_DIRECT] = 	ST_STR_D,
	[ST_STR_D][CH_COMMENT] = 	ST_STR_D,
	[ST_STR_D][CH_PLUS] = 		ST_STR_D,
	[ST_STR_D][CH_MINUS] = 		ST_STR_D,
	[ST_STR_D][CH_DOT] = 		ST_STR_D,
	[ST_STR_D][CH_QUOTE] = 		ST_STR_D,

	[ST_STR_D][CH_DQUOTE] = 	ST_SPACE,

	/*
	** ========================================================================
	** 			Eof handle
	** ========================================================================
	*/
	[ST_INIT ... ST_INVALID_END - 1][CH_EOF] = ST_EOF,
};

typedef struct		s_state_machine {
	t_state			state;
	char			*path;
	char			*origin;
	char			*ptr;
	t_word_state	prev_state;
	t_word_state	next_state;
	bool			inword;
	size_t			anchor;
	size_t			counter;
	size_t			line;
	size_t			line_offset;
	size_t			column;
}					t_state_machine;

static t_token_type	g_token_conv[ST_MAX] = {
	// Delimiters
	[ST_SEPARATOR] = TOK_SEPARATOR,
	[ST_NEWLINE] = TOK_NEWLINE,
	[ST_EOF] = TOK_NEWLINE,
	// Operators
	[ST_PLUS] = TOK_OP_ADD,
	[ST_MULT] = TOK_OP_MULT,
	[ST_MINUS] = TOK_OP_SUB,
	[ST_DIVIDE] = TOK_OP_DIV,

	[ST_AND_BIT] = TOK_OP_AND_BIT,
	[ST_XOR_BIT] = TOK_OP_XOR_BIT,
	[ST_OR_BIT] = TOK_OP_OR_BIT,
	[ST_AND_LOG] = TOK_OP_AND_LOG,
	[ST_XOR_LOG] = TOK_OP_XOR_LOG,
	[ST_OR_LOG] = TOK_OP_OR_LOG,

	[ST_RSHIFT] = TOK_OP_RSHIFT,
	[ST_LSHIFT] = TOK_OP_LSHIFT,

	[ST_NOT] = TOK_OP_NOT,
	[ST_BRACE_OPEN] = TOK_EXPR_BRC_OPEN,
	[ST_BRACE_CLOSE] = TOK_EXPR_BRC_CLOSE,
};

static inline void	_reg_double_operator(t_state_machine *m, t_dynarray *tokens) {
	/* printf(" └>\t\e[1;36mDouble operator: '%.*s'\e[0m\n", 2, m->ptr - 1); */
	ft_dynarray_push(tokens, &(t_token){
			.type = g_token_conv[m->next_state],
			.line = m->line,
			.col = m->column - 1,
			.len = 2,
			.str = m->ptr - 1,
	}, sizeof(t_token));
}

static inline void	_reg_simple_operator(t_state_machine *m, t_dynarray *tokens) {
	/* printf(" └>\t\e[1;36mSimple operator: '%.*s'\e[0m\n", 1, m->ptr - 1); */
	ft_dynarray_push(tokens, &(t_token){
			.type = g_token_conv[m->prev_state],
			.line = m->line,
			.col = m->column - 1,
			.len = 1,
			.str = m->ptr - 1,
	}, sizeof(t_token));
}

static inline void	_reg_simple_token(t_state_machine *m, t_dynarray *tokens) {
	/* printf(" └>\t\e[1;36mSimple token: %s\e[0m\n", get_printable(*m->ptr)); */
	ft_dynarray_push(tokens, &(t_token){
			.type = g_token_conv[m->next_state],
			.line = m->line,
			.col = m->column,
			.len = 1,
			.str = m->ptr,
	}, sizeof(t_token));
}

static inline void	_reg_update_counter(t_state_machine *m, t_dynarray *tokens) {
	m->line++;
	m->line_offset = m->ptr - m->origin;
	m->column = 0;
}

static inline void	_reg_new_line(t_state_machine *m, t_dynarray *tokens) {
	/* printf(" └>\t\e[1;35mNewline\e[0m\n"); */
	_reg_simple_token(m, tokens);
	_reg_update_counter(m, tokens);
}

static inline void	_reg_start_word(t_state_machine *m, t_dynarray *tokens) {
	/* printf(" └>\t\e[1;32mStart of word\e[0m\n"); */
	m->inword = true;
	m->anchor = m->ptr - m->origin;
	m->counter = 1;
}

static inline void	_reg_in_word(t_state_machine *m, t_dynarray *tokens) {
	/* printf(" └>\t\e[1;33mInside a word\e[0m\n"); */
	++m->counter;
}

static inline void	_reg_end_word(t_state_machine *m, t_dynarray *tokens) {
	t_token			*token;

	/* printf(" └>\t\e[1;34mEnd of word\e[0m\n"); */
	m->inword = false;
	token = &(t_token){
		.word = (t_token_word){
			.type = TOK_WORD,
				.line = m->line,
				.col = m->column - m->counter,
				.str = m->origin + m->anchor,
				.len = m->counter,
		}
	};
	convert_to_register(token);
	ft_dynarray_push(tokens, token, sizeof(t_token));
}

static inline void	_reg_end_dir(t_state_machine *m, t_dynarray *tokens) {
	char			*tmp;
	long			i;

	/* printf(" └>\t\e[1;33mEnd of dir\e[0m\n"); */
	m->inword = false;
	i = ft_strtol(m->origin + m->anchor, &tmp, 10);

	if (i > INT32_MAX || i < INT32_MIN
			|| !ft_strnequ(ft_ltostr((int32_t)i, 10, false), m->origin + m->anchor, m->counter))
	{
		print((t_print){
				.flags = (t_print_flags){.no_exit = true},
				.printer = compiler_printer, .level = LOG_CRIT,
				.data = &(t_print_compiler){
					.target = &(t_token){
						.loffset = 1,
						.len = m->counter,
						.col = m->column - m->counter,
						.line = m->line
					},
					.file = m->origin,
					.file_path = m->path,
					.message = "value too large for direct number.",
				}
		});
		m->state.error = true;
		m->state.errors++;
	}

	ft_dynarray_push(tokens, &(t_token){
			.number = (t_token_num){
				.type = TOK_DIRECT_NUMBER,
				.line = m->line,
				.col = m->column - m->counter,
				.str = m->origin + m->anchor,
				.len = m->counter,
				.loffset = 1,
				.num = i,
			},
	}, sizeof(t_token));
}

static inline void	_reg_end_num(t_state_machine *m, t_dynarray *tokens) {
	char			*tmp;
	int_least64_t	i;
	t_token			token;

	/* printf(" └>\t\e[1;33mEnd of num\e[0m\n"); */
	m->inword = false;
	i = ft_strtol(m->origin + m->anchor, &tmp, 10);
	token = (t_token){
			.number = (t_token_num){
				.type = TOK_NUMBER,
				.line = m->line,
				.col = m->column - m->counter,
				.str = m->origin + m->anchor,
				.len = m->counter,
				.num = i,
			},
	};
	if (i > INT32_MAX || i < INT32_MIN || m->counter > 13)
	{
		print((t_print){
				.flags = (t_print_flags){.no_exit = true},
				.printer = compiler_printer, .level = LOG_CRIT,
				.data = &(t_print_compiler){
					.target = &(t_token){
						.len = m->counter, .col = m->column - m->counter, .line = m->line
					},
					.file = m->origin,
					.file_path = m->path,
					.message = "value too large even for a \e[1;30;4m"
					"direct number\e[0m. "
					"\e[1;30m[\e[0;35m-Wauto-cast\e[0;1;30m]\e[0m",
				}
		});
		m->state.error = true;
		m->state.errors++;
		return ;
	}
	if (i > INT16_MAX || i < INT16_MIN
			|| !ft_strnequ(ft_ltostr((int16_t)i, 10, false), m->origin + m->anchor, m->counter))
	{
		tmp = ft_strndup(m->origin + m->anchor, m->counter);
		print((t_print){
				.flags = (t_print_flags){.no_exit = true},
				.printer = compiler_printer, .level = LOG_WARN,
				.data = &(t_print_compiler){
					.target = &(t_token){
						.len = m->counter, .col = m->column - m->counter, .line = m->line
					},
					.file = m->origin,
					.file_path = m->path,
					.message = "value too large for a \e[1;4;30mnumber\e[0m, "
					"converting to \e[1;4;30mdirect number\e[0m. "
					"\e[1;30m[\e[0;35m-Wauto-cast\e[0;1;30m]\e[0m",
					.suggest = ft_strajoin(2, "%", tmp),
					.suggest_offset = -1,
				}
		});
		free(tmp);
		token.type = TOK_DIRECT_NUMBER;
		m->state.warnings++;
	}
	ft_dynarray_push(tokens, &token, sizeof(t_token));
}

static inline void	_reg_end_key(t_state_machine *m, t_dynarray *tokens) {
	/* printf(" └>\t\e[1;36mEnd of key\e[0m\n"); */
	m->inword = false;
	ft_dynarray_push(tokens, &(t_token){
			.word = (t_token_word){
				.type = TOK_KEY_REF,
				.line = m->line,
				.roffset = 1,
				.col = m->column - m->counter - 1,
				.str = m->origin + m->anchor,
				.len = m->counter,
			},
	}, sizeof(t_token));
}

static inline void	_reg_start_ref(t_state_machine *m, t_dynarray *tokens) {
	/* printf(" └>\t\e[1;36mStart of ref\e[0m\n"); */
	m->inword = true;
	m->anchor = m->ptr - m->origin + 1;
	m->counter = 0;
}

static inline void	_reg_end_dir_ref(t_state_machine *m, t_dynarray *tokens) {
	/* printf(" └>\t\e[1;36mEnd of direct ref\e[0m\n"); */
	m->inword = false;
	ft_dynarray_push(tokens, &(t_token){
			.word = (t_token_word){
				.type = TOK_DIRECT_REF,
				.line = m->line,
				.col = m->column - m->counter,
				.loffset = 2,
				.str = m->origin + m->anchor,
				.len = m->counter,
			},
	}, sizeof(t_token));
}

static inline void	_reg_end_ind_ref(t_state_machine *m, t_dynarray *tokens) {
	/* printf(" └>\t\e[1;36mEnd of indirect ref\e[0m\n"); */
	m->inword = false;
	ft_dynarray_push(tokens, &(t_token){
			.word = (t_token_word){
				.type = TOK_INDIRECT_REF,
				.line = m->line,
				.col = m->column - m->counter,
				.loffset = 1,
				.str = m->origin + m->anchor,
				.len = m->counter,
			},
	}, sizeof(t_token));
}

static inline void	_reg_start_prop_name(t_state_machine *m, t_dynarray *tokens) {
	/* printf(" └>\t\e[1;31mStart of prop name\e[0m\n"); */
	m->inword = true;
	m->anchor = m->ptr - m->origin;
	m->counter = 1;
}

static inline void	_reg_end_prop_name(t_state_machine *m, t_dynarray *tokens) {
	/* printf(" └>\t\e[1;31mEnd of prop name\e[0m\n"); */
	m->inword = false;
	ft_dynarray_push(tokens, &(t_token){
			.word = (t_token_word){
				.type = TOK_PROP,
				.line = m->line,
				.col = m->column - m->counter,
				.loffset = 1,
				.str = m->origin + m->anchor,
				.len = m->counter,
			},
	}, sizeof(t_token));
}

static inline void	_reg_start_string(t_state_machine *m, t_dynarray *tokens) {
	/* printf(" └>\t\e[1;31mStart of string\e[0m\n"); */
	m->inword = true;
	m->anchor = m->ptr - m->origin + 1;
	m->counter = 0;
}

static inline void	_reg_end_string(t_state_machine *m, t_dynarray *tokens) {
	/* printf(" └>\t\e[1;31mEnd of string\e[0m\n"); */
	m->inword = false;
	ft_dynarray_push(tokens, &(t_token){
			.word = (t_token_word){
				.type = TOK_STRING,
				.line = m->line,
				.col = m->column - m->counter,
				.loffset = 1,
				.roffset = 1,
				.str = m->origin + m->anchor,
				.len = m->counter,
			},
	}, sizeof(t_token));
}

void				(*g_single_func[ST_MAX])(t_state_machine *, t_dynarray *) = {
	[ST_PLUS] = 				&_reg_simple_token,
	[ST_MINUS] = 				&_reg_simple_token,
	[ST_MULT] = 				&_reg_simple_token,
	[ST_DIVIDE] = 				&_reg_simple_token,

	[ST_AND_LOG] = 				&_reg_double_operator,
	[ST_XOR_LOG] = 				&_reg_double_operator,
	[ST_OR_LOG] = 				&_reg_double_operator,

	[ST_RSHIFT] = 				&_reg_double_operator,
	[ST_LSHIFT] = 				&_reg_double_operator,

	[ST_NOT] = 					&_reg_simple_token,
	[ST_BRACE_OPEN] = 			&_reg_simple_token,
	[ST_BRACE_CLOSE] = 			&_reg_simple_token,

	[ST_NEWLINE] = 				&_reg_new_line,
	[ST_SEPARATOR] = 			&_reg_simple_token,
	[ST_EOF] = 					&_reg_simple_token,
};

void				(*g_word_func[ST_MAX][ST_MAX][2])(t_state_machine *, t_dynarray *) = {
	/*
	 * Operators token creation
	 */
	[ST_AND_BIT][ST_INIT ... ST_WORD - 1] = 			{&_reg_simple_operator},
	[ST_AND_BIT][ST_NUMBER + 1 ... ST_AND_LOG - 1] = 	{&_reg_simple_operator},
	[ST_AND_BIT][ST_AND_LOG + 1 ... ST_MAX - 1] = 		{&_reg_simple_operator},
	[ST_XOR_BIT][ST_INIT ... ST_WORD - 1] = 			{&_reg_simple_operator},
	[ST_XOR_BIT][ST_NUMBER + 1 ... ST_XOR_LOG - 1] = 	{&_reg_simple_operator},
	[ST_XOR_BIT][ST_XOR_LOG + 1 ... ST_MAX - 1] = 		{&_reg_simple_operator},
	[ST_OR_BIT][ST_INIT ... ST_WORD - 1] = 				{&_reg_simple_operator},
	[ST_OR_BIT][ST_NUMBER + 1 ... ST_OR_LOG - 1] = 		{&_reg_simple_operator},
	[ST_OR_BIT][ST_OR_LOG + 1 ... ST_MAX - 1] = 		{&_reg_simple_operator},

	/*
	 * Word tokens creation
	 */
	[ST_INIT][ST_WORD] = 			{&_reg_start_word},

	[ST_PLUS][ST_WORD] = 			{&_reg_start_word},
	[ST_MINUS][ST_WORD] = 			{&_reg_start_word},
	[ST_MULT][ST_WORD] = 			{&_reg_start_word},
	[ST_DIVIDE][ST_WORD] = 			{&_reg_start_word},
	[ST_AND_BIT][ST_WORD] = 		{&_reg_simple_operator, &_reg_start_word},
	[ST_XOR_BIT][ST_WORD] = 		{&_reg_simple_operator, &_reg_start_word},
	[ST_OR_BIT][ST_WORD] = 			{&_reg_simple_operator, &_reg_start_word},
	[ST_AND_LOG][ST_WORD] = 		{&_reg_start_word},
	[ST_XOR_LOG][ST_WORD] = 		{&_reg_start_word},
	[ST_OR_LOG][ST_WORD] = 			{&_reg_start_word},
	[ST_RCHEV][ST_WORD] = 			{&_reg_start_word},
	[ST_LCHEV][ST_WORD] = 			{&_reg_start_word},
	[ST_RSHIFT][ST_WORD] = 			{&_reg_start_word},
	[ST_LSHIFT][ST_WORD] = 			{&_reg_start_word},
	[ST_NOT][ST_WORD] = 			{&_reg_start_word},
	[ST_BRACE_OPEN][ST_WORD] = 		{&_reg_start_word},
	[ST_BRACE_CLOSE][ST_WORD] = 	{&_reg_start_word},

	[ST_SEPARATOR][ST_WORD] = 		{&_reg_start_word},
	[ST_SPACE][ST_WORD] = 			{&_reg_start_word},
	[ST_NEWLINE][ST_WORD] = 		{&_reg_start_word},

	[ST_WORD][ST_WORD] = 			{&_reg_in_word},
	[ST_NUMBER][ST_WORD] = 			{&_reg_in_word},

	[ST_WORD][ST_OPERATOR] = 		{&_reg_end_word},
	[ST_WORD][ST_SEPARATOR] = 		{&_reg_end_word},
	[ST_NUMBER][ST_SEPARATOR] = 	{&_reg_end_word},
	[ST_WORD][ST_SPACE] = 			{&_reg_end_word},
	[ST_WORD][ST_EOF] = 			{&_reg_end_word},

	/*
	 * Number tokens creation
	 */

	[ST_INIT][ST_NUMBER] = 			{&_reg_start_word},

	[ST_PLUS][ST_NUMBER] = 			{&_reg_start_word},
	[ST_MINUS][ST_NUMBER] = 		{&_reg_start_word},
	[ST_MULT][ST_NUMBER] = 			{&_reg_start_word},
	[ST_DIVIDE][ST_NUMBER] = 		{&_reg_start_word},
	[ST_AND_BIT][ST_NUMBER] = 		{&_reg_simple_operator, &_reg_start_word},
	[ST_XOR_BIT][ST_NUMBER] = 		{&_reg_simple_operator, &_reg_start_word},
	[ST_OR_BIT][ST_NUMBER] = 		{&_reg_simple_operator, &_reg_start_word},
	[ST_AND_LOG][ST_NUMBER] = 		{&_reg_start_word},
	[ST_XOR_LOG][ST_NUMBER] = 		{&_reg_start_word},
	[ST_OR_LOG][ST_NUMBER] = 		{&_reg_start_word},
	[ST_RCHEV][ST_NUMBER] = 		{&_reg_start_word},
	[ST_LCHEV][ST_NUMBER] = 		{&_reg_start_word},
	[ST_RSHIFT][ST_NUMBER] = 		{&_reg_start_word},
	[ST_LSHIFT][ST_NUMBER] = 		{&_reg_start_word},
	[ST_NOT][ST_NUMBER] = 			{&_reg_start_word},
	[ST_BRACE_OPEN][ST_NUMBER] = 	{&_reg_start_word},
	[ST_BRACE_CLOSE][ST_NUMBER] = 	{&_reg_start_word},

	[ST_SEPARATOR][ST_NUMBER] = 	{&_reg_start_word},
	[ST_SPACE][ST_NUMBER] = 		{&_reg_start_word},
	[ST_NEWLINE][ST_NUMBER] = 		{&_reg_start_word},

	[ST_NUMBER][ST_NUMBER] = 		{&_reg_in_word},

	[ST_NUMBER][ST_SPACE] = 		{&_reg_end_num},

	[ST_NUMBER][ST_OPERATOR] = 		{&_reg_end_num},

	[ST_NUMBER][ST_NEWLINE] = 		{&_reg_end_num},
	[ST_NUMBER][ST_EOF] = 			{&_reg_end_num},

	/*
	 * Direct number tokens creation
	 */

	[ST_DIRECT][ST_DIRECT_NUMBER] = 		{&_reg_start_word},

	[ST_DIRECT_NUMBER][ST_DIRECT_NUMBER] = 	{&_reg_in_word},

	[ST_DIRECT_NUMBER][ST_SPACE] = 			{&_reg_end_dir},
	[ST_DIRECT_NUMBER][ST_SEPARATOR] = 		{&_reg_end_dir},
	[ST_DIRECT_NUMBER][ST_EOF] = 			{&_reg_end_dir},
	[ST_DIRECT_NUMBER][ST_NEWLINE] = 		{&_reg_end_dir},

	[ST_DIRECT_NUMBER][ST_PLUS] = 			{&_reg_end_dir},
	[ST_DIRECT_NUMBER][ST_MINUS] = 			{&_reg_end_dir},
	[ST_DIRECT_NUMBER][ST_DIVIDE] = 		{&_reg_end_dir},
	[ST_DIRECT_NUMBER][ST_MULT] = 			{&_reg_end_dir},

	[ST_DIRECT_NUMBER][ST_AND_LOG] = 		{&_reg_end_dir},
	[ST_DIRECT_NUMBER][ST_XOR_LOG] = 		{&_reg_end_dir},
	[ST_DIRECT_NUMBER][ST_OR_LOG] = 		{&_reg_end_dir},
	[ST_DIRECT_NUMBER][ST_AND_BIT] = 		{&_reg_end_dir},
	[ST_DIRECT_NUMBER][ST_XOR_BIT] = 		{&_reg_end_dir},
	[ST_DIRECT_NUMBER][ST_OR_BIT] = 		{&_reg_end_dir},

	[ST_DIRECT_NUMBER][ST_RSHIFT] = 		{&_reg_end_dir},
	[ST_DIRECT_NUMBER][ST_LSHIFT] = 		{&_reg_end_dir},

	[ST_DIRECT_NUMBER][ST_NOT] = 			{&_reg_end_dir},
	[ST_DIRECT_NUMBER][ST_BRACE_OPEN] = 	{&_reg_end_dir},
	[ST_DIRECT_NUMBER][ST_BRACE_CLOSE] = 	{&_reg_end_dir},

	/*
	 * Separator tokens creation
	 */

	/*
	 * Label tokens generation
	 */

	[ST_SPACE][ST_INDIRECT_LABEL] = 			{&_reg_start_ref},
	[ST_SEPARATOR][ST_INDIRECT_LABEL] = 		{&_reg_start_ref},
	[ST_NEWLINE][ST_INDIRECT_LABEL] = 			{&_reg_start_ref},

	[ST_DIRECT][ST_DIRECT_LABEL] = 				{&_reg_start_ref},

	[ST_PLUS][ST_INDIRECT_LABEL] = 				{&_reg_start_ref},
	[ST_MINUS][ST_INDIRECT_LABEL] = 			{&_reg_start_ref},
	[ST_MULT][ST_INDIRECT_LABEL] = 				{&_reg_start_ref},
	[ST_DIVIDE][ST_INDIRECT_LABEL] = 			{&_reg_start_ref},
	[ST_BRACE_OPEN][ST_INDIRECT_LABEL] = 		{&_reg_start_ref},
	[ST_BRACE_CLOSE][ST_INDIRECT_LABEL] = 		{&_reg_start_ref},

	[ST_INDIRECT_LABEL][ST_INDIRECT_LABEL] = 	{&_reg_in_word},
	[ST_DIRECT_LABEL][ST_DIRECT_LABEL] = 		{&_reg_in_word},

	[ST_LABEL_KEY][ST_NEWLINE] = 				{&_reg_end_key},
	[ST_LABEL_KEY][ST_SPACE] = 					{&_reg_end_key},

	[ST_DIRECT_LABEL][ST_NEWLINE] = 			{&_reg_end_dir_ref},
	[ST_DIRECT_LABEL][ST_SEPARATOR] = 			{&_reg_end_dir_ref},
	[ST_DIRECT_LABEL][ST_SPACE] = 				{&_reg_end_dir_ref},
	[ST_DIRECT_LABEL][ST_EOF] = 				{&_reg_end_dir_ref},

	[ST_DIRECT_LABEL][ST_PLUS] = 				{&_reg_end_ind_ref},
	[ST_DIRECT_LABEL][ST_MINUS] = 				{&_reg_end_ind_ref},
	[ST_DIRECT_LABEL][ST_MULT] = 				{&_reg_end_ind_ref},
	[ST_DIRECT_LABEL][ST_DIVIDE] = 				{&_reg_end_ind_ref},
	[ST_DIRECT_LABEL][ST_BRACE_OPEN] = 			{&_reg_end_ind_ref},
	[ST_DIRECT_LABEL][ST_BRACE_CLOSE] = 		{&_reg_end_ind_ref},

	[ST_INDIRECT_LABEL][ST_NEWLINE] = 			{&_reg_end_ind_ref},
	[ST_INDIRECT_LABEL][ST_SEPARATOR] = 		{&_reg_end_ind_ref},
	[ST_INDIRECT_LABEL][ST_SPACE] = 			{&_reg_end_ind_ref},
	[ST_INDIRECT_LABEL][ST_EOF] = 				{&_reg_end_ind_ref},

	[ST_INDIRECT_LABEL][ST_PLUS] = 				{&_reg_end_ind_ref},
	[ST_INDIRECT_LABEL][ST_MINUS] = 			{&_reg_end_ind_ref},
	[ST_INDIRECT_LABEL][ST_MULT] = 				{&_reg_end_ind_ref},
	[ST_INDIRECT_LABEL][ST_DIVIDE] = 			{&_reg_end_ind_ref},

	[ST_INDIRECT_LABEL][ST_AND_BIT] = 			{&_reg_end_ind_ref},
	[ST_INDIRECT_LABEL][ST_XOR_BIT] = 			{&_reg_end_ind_ref},
	[ST_INDIRECT_LABEL][ST_OR_BIT] = 			{&_reg_end_ind_ref},
	[ST_INDIRECT_LABEL][ST_AND_LOG] = 			{&_reg_end_ind_ref},
	[ST_INDIRECT_LABEL][ST_XOR_LOG] = 			{&_reg_end_ind_ref},
	[ST_INDIRECT_LABEL][ST_OR_LOG] = 			{&_reg_end_ind_ref},

	[ST_INDIRECT_LABEL][ST_LSHIFT] = 			{&_reg_end_ind_ref},
	[ST_INDIRECT_LABEL][ST_RSHIFT] = 			{&_reg_end_ind_ref},

	[ST_INDIRECT_LABEL][ST_NOT] = 				{&_reg_end_ind_ref},
	[ST_INDIRECT_LABEL][ST_BRACE_OPEN] = 		{&_reg_end_ind_ref},
	[ST_INDIRECT_LABEL][ST_BRACE_CLOSE] = 		{&_reg_end_ind_ref},

/*
** Properties
*/

	[ST_PROP][ST_PROP_NAME] = 					{&_reg_start_prop_name},
	[ST_PROP_NAME][ST_PROP_NAME] = 				{&_reg_in_word},
	[ST_PROP_NAME][ST_SPACE] = 					{&_reg_end_prop_name},
	[ST_PROP_NAME][ST_EOF] = 					{&_reg_end_prop_name},
	[ST_PROP_NAME][ST_NEWLINE] = 				{&_reg_end_prop_name},

/*
** Strings
*/

	[ST_PROP_NAME][ST_STR_S] = 					{&_reg_start_string},
	[ST_INIT][ST_STR_S] = 						{&_reg_start_string},
	[ST_WORD][ST_STR_S] = 						{&_reg_start_string},
	[ST_NUMBER][ST_STR_S] = 					{&_reg_start_string},
	[ST_DIRECT_NUMBER][ST_STR_S] = 				{&_reg_start_string},
	[ST_INDIRECT_LABEL][ST_STR_S] = 			{&_reg_start_string},
	[ST_DIRECT_LABEL][ST_STR_S] = 				{&_reg_start_string},
	[ST_LABEL_KEY][ST_STR_S] = 					{&_reg_start_string},
	[ST_SPACE][ST_STR_S] = 						{&_reg_start_string},
	[ST_NEWLINE][ST_STR_S] = 					{&_reg_start_string},
	[ST_SEPARATOR][ST_STR_S] = 					{&_reg_start_string},
	[ST_DIRECT][ST_STR_S] = 					{&_reg_start_string},
	[ST_PROP][ST_STR_S] = 						{&_reg_start_string},

	[ST_STR_S][ST_STR_S] = 						{&_reg_in_word},
	[ST_STR_S][ST_SPACE] = 						{&_reg_end_string},

	[ST_PROP_NAME][ST_STR_D] = 					{&_reg_start_string},
	[ST_INIT][ST_STR_D] = 						{&_reg_start_string},
	[ST_WORD][ST_STR_D] = 						{&_reg_start_string},
	[ST_NUMBER][ST_STR_D] = 					{&_reg_start_string},
	[ST_DIRECT_NUMBER][ST_STR_D] = 				{&_reg_start_string},
	[ST_INDIRECT_LABEL][ST_STR_D] = 			{&_reg_start_string},
	[ST_DIRECT_LABEL][ST_STR_D] = 				{&_reg_start_string},
	[ST_LABEL_KEY][ST_STR_D] = 					{&_reg_start_string},
	[ST_SPACE][ST_STR_D] = 						{&_reg_start_string},
	[ST_NEWLINE][ST_STR_D] = 					{&_reg_start_string},
	[ST_SEPARATOR][ST_STR_D] = 					{&_reg_start_string},
	[ST_DIRECT][ST_STR_D] = 					{&_reg_start_string},
	[ST_PROP][ST_STR_D] = 						{&_reg_start_string},

	[ST_STR_D][ST_STR_D] = 						{&_reg_in_word},
	[ST_STR_D][ST_SPACE] = 						{&_reg_end_string},
};

typedef enum			e_lexical_error
{
	LEX_ERR_NOPE,
	LEX_ERR_TYPO_DIRECT,
	LEX_ERR_TYPO_LABEL,
	LEX_ERR_TYPO_SEPARATOR,
	LEX_ERR_MAX
}						t_lexical_error;

static t_lexical_error	g_get_error_type[TOK_MAX][ST_MAX][CH_MAX] = {
	[TOK_WORD][ST_SPACE][CH_DOT] = LEX_ERR_TYPO_SEPARATOR,
	[TOK_WORD][ST_WORD][CH_DOT] = LEX_ERR_TYPO_SEPARATOR,
	[TOK_WORD][ST_WORD][CH_SPACE] = LEX_ERR_TYPO_SEPARATOR,
	[TOK_WORD][ST_SPACE][CH_NONE] = LEX_ERR_TYPO_SEPARATOR,
	[TOK_WORD][ST_WORD][CH_NONE] = LEX_ERR_TYPO_SEPARATOR,
	[TOK_REGISTER][ST_SPACE][CH_DOT] = LEX_ERR_TYPO_SEPARATOR,
	[TOK_REGISTER][ST_WORD][CH_NONE] = LEX_ERR_TYPO_SEPARATOR,
	[TOK_ANY][ST_WORD][CH_MINUS] = LEX_ERR_TYPO_LABEL,
	[TOK_ANY][ST_WORD][CH_PLUS] = LEX_ERR_TYPO_LABEL,
	[TOK_ANY][ST_WORD][CH_SEPARATOR] = LEX_ERR_TYPO_LABEL,
};

static char				*g_error_known[LEX_ERR_MAX] = {
	[LEX_ERR_TYPO_SEPARATOR] = "Unexpected '<tok>', maybe try putting a ','.",
	[LEX_ERR_TYPO_LABEL] = "Unexpected '<tok>', maybe try putting a ':'.",
	[LEX_ERR_TYPO_DIRECT] = "Unexpected '<tok>', maybe try putting a '%'.",
};

static void				_print_lexical_error(
		t_state_machine m,
		t_dynarray *tokens,
		t_binary *binary
)
{
	t_word_state		next;
	t_token				*token;
	t_token_type		prev;
	char				*message;

	token = ft_dynarray_stack_top(tokens, sizeof(t_token));
	if ((next = g_word_transition[ST_INIT][get_char_class[(uint8_t)m.ptr[1]]])
			&& token && (prev = token->type))
	{
		/* printf("[%c] %s %s\n", *m.ptr, g_token_types[prev], g_word_str[next]); */
		message = g_error_known[g_get_error_type[prev][next][get_char_class[(uint8_t)m.ptr[0]]]];
	}
	else
		message = "Unexpected syntax.";
	message = replace_format_placeholders(message, 1,
			(t_print_pattern){
				.pattern = "<tok>", .plen = 5,
				.replace = m.ptr,
				.rlen = 1
			});
	print((t_print){
			.flags = (t_print_flags){.no_exit = true},
			.printer = compiler_printer, .level = LOG_CRIT,
			.data = &(t_print_compiler){
				.target = &(t_token){
					.len = 1, .col = m.column, .line = m.line
				},
				.file = m.origin,
				.file_path = binary->source_path,
				.message = message
			}
	});
}

t_state				tokenize(
		t_dynarray *tokens,
		t_binary *binary
)
{
	t_state_machine	m;

	m = (t_state_machine){
			.path = binary->source_path,
			.origin = binary->source_content,
			.ptr = binary->source_content,
			.line = 1,
			.column = 1,
			.prev_state = ST_INIT,
			.state = (t_state){},
	};
	while (!m.state.error && m.prev_state != ST_EOF) {
		m.next_state = g_word_transition[m.prev_state][get_char_class[(uint8_t)*m.ptr]];
		/* printf("col: %4lu ctr: %4lu ", m.column, m.counter); */
		/* printf("%-4s states: %s -> %s\n", get_printable(*m.ptr), g_word_str[m.prev_state], g_word_str[m.next_state]); */
		if (g_word_func[m.prev_state][m.next_state][0])
			g_word_func[m.prev_state][m.next_state][0](&m, tokens);
		if (g_word_func[m.prev_state][m.next_state][1])
			g_word_func[m.prev_state][m.next_state][1](&m, tokens);
		if (g_single_func[m.next_state])
			g_single_func[m.next_state](&m, tokens);
		m.prev_state = m.next_state;
		if (m.prev_state == ST_ERROR)
			break ;
		m.column += (*m.ptr == '\t') ? TABSTOP - ((m.column - 1) % TABSTOP) : 1;
		++m.ptr;
	}
	if (!m.state.error && (m.state.error = (m.next_state == ST_ERROR)))
		_print_lexical_error(m, tokens, binary);
	/* print_tokens(tokens); */
	return (m.state);
}
