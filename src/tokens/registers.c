/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   registers.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/05 18:05:01 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/05 19:48:03 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "compiler.h"
#include "registers.h"

static t_state_reg	g_state_reg[REG_MAX][256] = {
	[REG_INIT]['r'] = REG_R,
	[REG_R]['0'] = REG_R0,
	[REG_R ... REG_R0]['1'] = REG_R1,
	[REG_R ... REG_R0]['2'] = REG_R2,
	[REG_R ... REG_R0]['3'] = REG_R3,
	[REG_R ... REG_R0]['4'] = REG_R4,
	[REG_R ... REG_R0]['5'] = REG_R5,
	[REG_R ... REG_R0]['6'] = REG_R6,
	[REG_R ... REG_R0]['7'] = REG_R7,
	[REG_R ... REG_R0]['8'] = REG_R8,
	[REG_R ... REG_R0]['9'] = REG_R9,
	[REG_R1]['0'] = REG_R10,
	[REG_R1]['1'] = REG_R11,
	[REG_R1]['2'] = REG_R12,
	[REG_R1]['3'] = REG_R13,
	[REG_R1]['4'] = REG_R14,
	[REG_R1]['5'] = REG_R15,
	[REG_R1]['6'] = REG_R16,
};

/*
** ============================================================================
** 		Try to convert token to register type according to:
** 			keyword syntax : 'r0?([0-9]|1[0-6])'
** 		If it can't it leaves the token as is and returns an error
** ============================================================================
*/

t_state				convert_to_register(t_token *word)
{
	t_state_reg		state;
	size_t			i;

	i = 0;
	state = REG_INIT;
	while (i < word->len) {
		state = g_state_reg[state][(unsigned char)word->word.str[i]];
		++i;
	}
	if (state == REG_ERR || state >= REG_INIT)
		return ((t_state){.error = true, .errors = 1});
	word->type = TOK_REGISTER;
	word->reg.id = (t_reg_id)state;
	return ((t_state){});
}
