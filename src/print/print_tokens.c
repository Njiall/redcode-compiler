/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_tokens.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/28 14:35:10 by mbeilles          #+#    #+#             */
/*   Updated: 2019/11/02 10:53:45 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdint.h>
#include "dynarray.h"
#include "compiler.h"
#include "print.h"

static char			*g_token_types[TOK_MAX] = {
	[TOK_NONE] = 			"none",
	[TOK_WORD] = 			"\e[1;34mword\e[0m",
	[TOK_NUMBER] = 			"\e[1;33mnumber\e[0m",
	[TOK_DIRECT_NUMBER] = 	"\e[1;33mdirect number\e[0m",
	[TOK_NEWLINE] = 		"\e[1;35mnewline\e[0m",
	[TOK_SEPARATOR] = 		"\e[1;32mseparator\e[0m",
	[TOK_INDIRECT_REF] = 	"\e[1;36mindirect reference\e[0m",
	[TOK_DIRECT_REF] = 		"\e[1;36mdirect reference\e[0m",
	[TOK_KEY_REF] = 		"\e[1;36mkey\e[0m",
	[TOK_PROP] = 			"\e[1;31mproperty\e[0m",
	[TOK_STRING] = 			"\e[1;31mstring\e[0m",
	[TOK_REGISTER] = 		"\e[1;34mregister\e[0m",
	[TOK_OP_ADD] = 			"\e[1;32madd\e[0m",
	[TOK_OP_SUB] = 			"\e[1;32msub\e[0m",
	[TOK_OP_DIV] = 			"\e[1;32mdivide\e[0m",
	[TOK_OP_MULT] = 		"\e[1;32mmult\e[0m",

	[TOK_OP_AND_LOG] = 		"\e[1;32mand \e[0;40;32mlogic\e[0m",
	[TOK_OP_XOR_LOG] = 		"\e[1;32mxor \e[0;40;32mlogic\e[0m",
	[TOK_OP_OR_LOG] = 		"\e[1;32mor \e[0;40;32mlogic\e[0m",
	[TOK_OP_AND_BIT] = 		"\e[1;32mand \e[0;40;32mbitwise\e[0m",
	[TOK_OP_XOR_BIT] = 		"\e[1;32mxor \e[0;40;32mbitwise\e[0m",
	[TOK_OP_OR_BIT] = 		"\e[1;32mor \e[0;40;32mbitwise\e[0m",

	[TOK_OP_LSHIFT] = 		"\e[32mleft bit \e[1mshift\e[0m",
	[TOK_OP_RSHIFT] = 		"\e[32mright bit \e[1mshift\e[0m",

	[TOK_OP_NOT] = 			"\e[1;32mnot\e[0m",
	[TOK_EXPR_BRC_CLOSE] = 	"\e[1;32m)\e[0m",
	[TOK_EXPR_BRC_OPEN] = 	"\e[1;32m(\e[0m",
};

void				print_tokens(t_dynarray *tokens)
{
	t_token			*token;
	uint64_t		i;

	printf("Tokens: \e[1;2;34m[\e[0m\n");
	i = 0;
	while ((token = ft_dynarray_iterate(tokens, &i, sizeof(t_token)))) {
		printf("\t");
		print_token(token);
	}
	printf("\e[1;2;34m]\e[0m\n");
}
