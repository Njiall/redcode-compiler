/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_ast_node_tree.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 19:07:46 by mbeilles          #+#    #+#             */
/*   Updated: 2019/11/02 12:56:22 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "print.h"
#include "compiler.h"
#include "ast.h"

#include <stdio.h>

static char			*g_ast_types[AST_TYPE_MAX] = {
	[AST_TYPE_EXPR] = 			"Expression",
	[AST_TYPE_INSTRUCTION] = 	"Instruction",
	[AST_TYPE_KEY] = 			"Key",
	[AST_TYPE_TOKEN] = 			"Token",
};

static char						*g_expr_types[EXPR_TYPE_MAX] = {
	[EXPR_TYPE_OP] = "Operation",
	[EXPR_TYPE_EXPR] = "Sub expression",
	[EXPR_TYPE_NUMBER] = "Number",
	[EXPR_TYPE_LONG_NUMBER] = "Long number",
	[EXPR_TYPE_TOKEN] = "Token",
};

static char						*g_expr_op_type[EXPR_OP_MAX] = {
	[EXPR_OP_ADD] = "+",
	[EXPR_OP_SUB] = "-",
	[EXPR_OP_DIV] = "/",
	[EXPR_OP_MULT] = "*",
	[EXPR_OP_AND_BIT] = "&",
	[EXPR_OP_XOR_BIT] = "^",
	[EXPR_OP_OR_BIT] = "|",
	[EXPR_OP_AND_LOG] = "&&",
	[EXPR_OP_XOR_LOG] = "^^",
	[EXPR_OP_OR_LOG] = "||",
	[EXPR_OP_LSHIFT] = "<<",
	[EXPR_OP_RSHIFT] = ">>",
	[EXPR_OP_NOT] = "!",
};

void	print_ast_expr_tree_wrap(
		t_expr *expr,
		size_t scope
)
{
	size_t			i;

	for (i = 0; i < scope; i++)
		if (i & 1)
			printf("\e[40m  \e[0m");
		else
			printf("  ");
	if (i & 1)
		printf("\e[40m└>\e[0m ");
	else
		printf("└> ");
	if (expr->type == EXPR_TYPE_OP)
		printf("\e[1;32m%s\e[0m, ", g_expr_op_type[expr->op.op]);
	printf("%s", g_expr_types[expr->type]);
	printf(", args: %hhu", expr->len);
	printf(", token: ");
	print_token(&expr->token);
	for (int i = 0; i < expr->len; i++) {
		print_ast_expr_tree_wrap(expr->args + i, scope + 1);
	}
}

static inline void _print_scope(
		size_t scope,
		bool arrow
)
{
	size_t			i;
	// Prints spaces for scope tabulation
	for (i = 0; i < scope; i++)
		if (i & 1)
			printf("\e[40m  \e[0m");
		else
			printf("  ");
	// Prints node content
	if (arrow)
	{
		if (scope == 1)
		{
			if (i & 1)
				printf("\e[40;1m->\e[0m ");
			else
				printf("\e[1m->\e[0m ");
		}
		else
		{
			if (i & 1)
				printf("\e[40m└>\e[0m ");
			else
				printf("└> ");
		}
	}
	else
		printf("   ");
	fflush(stdout);
}

void				print_ast_node_tree_wrap(
		t_ast_node *node,
		size_t scope
)
{
	_print_scope(scope, true);
	switch (node->type) {
		case AST_TYPE_INSTRUCTION:
			printf("\e[1;34mInstruction\e[0m [\e[34m%.*s\e[0m]\n", (int)node->instruction.op->len,
					node->instruction.op->str);
			fflush(stdout);
			for (int i = 0; i < node->instruction.len; i++) {
				print_ast_node_tree_wrap(node->instruction.args + i, scope + 1);
			}
			break ;
		case AST_TYPE_KEY:
			printf("\e[1;36mKey\e[0;2m:\e[0m ");
			if (node->key.token)
				print_token(node->key.token);
			else
				printf("\e[1;2;34m{\e[0;34mstr\e[2m:\e[0;34m '\e[1m%.*s\e[0;34m'\e[1;2m}\e[0m\n", (int)node->key.len, node->key.key);
			break ;
		case AST_TYPE_EXPR:
			printf("\e[1;35mExpression\e[0m, token: ");
			print_token(&node->expr.target);
			print_ast_expr_tree_wrap(&node->expr.expr, scope + 1);
			break ;
		case AST_TYPE_TOKEN:
			if (node->token.token.type)
			{
				printf("\e[1;32mToken\e[0;2m:\e[0m ");
				print_token(&node->token.token);
			}
			else
				printf("\e[1;32mNo token\e[0m\n");
			break ;
		default:
			break ;
	}
}

void				print_ast_node_tree(t_ast_node *node)
{
	printf("%s node [\n", g_ast_types[node->type]);
	print_ast_node_tree_wrap(node, 1);
	printf("]\n");
}
