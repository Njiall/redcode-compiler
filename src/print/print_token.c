/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_token.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/28 14:35:10 by mbeilles          #+#    #+#             */
/*   Updated: 2019/11/02 17:10:54 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdint.h>
#include "compiler.h"
#include "print.h"

static char			*g_token_types[TOK_MAX] = {
	[TOK_NONE] = 			"none",
	[TOK_WORD] = 			"\e[1;34mword\e[0m",
	[TOK_NUMBER] = 			"\e[1;33mnumber\e[0m",
	[TOK_DIRECT_NUMBER] = 	"\e[1;33mdirect number\e[0m",
	[TOK_NEWLINE] = 		"\e[1;35mnewline\e[0m",
	[TOK_SEPARATOR] = 		"\e[1;32mseparator\e[0m",
	[TOK_INDIRECT_REF] = 	"\e[1;36mindirect reference\e[0m",
	[TOK_DIRECT_REF] = 		"\e[1;36mdirect reference\e[0m",
	[TOK_KEY_REF] = 		"\e[1;36mkey\e[0m",
	[TOK_PROP] = 			"\e[1;31mproperty\e[0m",
	[TOK_STRING] = 			"\e[1;31mstring\e[0m",
	[TOK_REGISTER] = 		"\e[1;34mregister\e[0m",
	[TOK_OP_ADD] = 			"\e[1;32madd\e[0m",
	[TOK_OP_SUB] = 			"\e[1;32msub\e[0m",
	[TOK_OP_DIV] = 			"\e[1;32mdivide\e[0m",
	[TOK_OP_MULT] = 		"\e[1;32mmult\e[0m",

	[TOK_OP_AND_LOG] = 		"\e[1;32mand \e[0;32mlogic\e[0m",
	[TOK_OP_XOR_LOG] = 		"\e[1;32mxor \e[0;32mlogic\e[0m",
	[TOK_OP_OR_LOG] = 		"\e[1;32mor \e[0;32mlogic\e[0m",
	[TOK_OP_AND_BIT] = 		"\e[1;32mand \e[0;32mbitwise\e[0m",
	[TOK_OP_XOR_BIT] = 		"\e[1;32mxor \e[0;32mbitwise\e[0m",
	[TOK_OP_OR_BIT] = 		"\e[1;32mor \e[0;32mbitwise\e[0m",

	[TOK_OP_LSHIFT] = 		"\e[32mleft bit \e[1mshift\e[0m",
	[TOK_OP_RSHIFT] = 		"\e[32mright bit \e[1mshift\e[0m",

	[TOK_OP_NOT] = 			"\e[1;32mnot\e[0m",
	[TOK_EXPR_BRC_CLOSE] = 	"\e[1;32mclose\e[0;32m expr\e[0m",
	[TOK_EXPR_BRC_OPEN] = 	"\e[1;32mopen\e[0;32m expr\e[0m",
};

static char			*get_printable(char c) {
	static char		s[4] = " ";
	switch (c) {
		case '\t':
			return ("\\t");
		case '\n':
			return ("\\n");
		case '\v':
			return ("\\v");
		case '\r':
			return ("\\r");
		case '\0':
			return ("eof");
		default:
			s[0] = c;
			return (s);
	}
}

void				print_token(t_token *token)
{
	if (!token)
	{
		printf("No token\n");
		return ;
	}
	printf("\e[1;2;34m{");
	printf("\e[0;1;33m%03lu\e[0;2m, \e[0;1;34m%03lu\e[0;2m", token->col, token->line);
	printf(", \e[0;1;31mtype\e[0;2m:\e[0m %s", g_token_types[token->type]);
	if (token->len > 1)
		printf(", str: '\e[1;30m%.*s\e[0m'", (int)token->len, token->str);
	else if (token->str)
		printf(", str: '\e[1;30m%s\e[0m'", get_printable(*token->str));
	switch (token->type) {
		case TOK_REGISTER:
			printf(", id: ");
			if (token->reg.id < 10)
				printf("\e[2m0\e[0m");
			printf("%d", token->reg.id);
			break ;
		case TOK_NUMBER:
		case TOK_DIRECT_NUMBER:
			printf(", number: '\e[33m%i\e[0m'", token->number.num);
			break ;
		default:
			break ;
	}
	printf("\e[1;2;34m}\e[0m\n");
}
