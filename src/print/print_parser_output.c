/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_parser_output.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: njiall <mbeilles@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/26 14:31:52 by njiall            #+#    #+#             */
/*   Updated: 2019/09/28 01:26:36 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

#include "parser.h"
#include "compiler.h"
#include "print.h"
#include "libft.h"

static char		*g_levels[LOG_MAX] = {
	[LOG_DEBUG] = "\e[1;34mdebug\e[0;1m:",
	[LOG_INFO] = "\e[1;34minfo\e[0;1m:",
	[LOG_WARN] = "\e[1;35mwarning\e[0;1m:",
	[LOG_CRIT] = "\e[1;31merror\e[0;1m:",
};

char			*get_compiler_print_format(
		t_print_level level,
		t_token *target,
		char *file_path,
		char *message
)
{
	t_dynarray	str;

	str = ft_dynarray_create_loc(0, 0);
	ft_dynarray_push_str(&str, "\e[1m");
	ft_dynarray_push_str(&str, file_path);
	if (target)
	{
		ft_dynarray_push(&str, ":", 1);
		ft_dynarray_push_str(&str, ft_ultostr(target->line, 10, 0));
		ft_dynarray_push(&str, ":", 1);
		ft_dynarray_push_str(&str,
				ft_ultostr(target->col - target->loffset, 10, 0));
	}
	ft_dynarray_push_str(&str, "\e[0m: ");
	ft_dynarray_push_str(&str, g_levels[level]);
	ft_dynarray_push(&str, " ", 1);
	if (message)
	{
		ft_dynarray_push_str(&str, message);
		ft_dynarray_push(&str, "\n", 1);
	}
	else
		ft_dynarray_push_str(&str, "Unexpected error occured.\n");
	ft_dynarray_push(&str, "\e[0m", 5);
	return ((char*)str.array);
}
