/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_compiler.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 19:55:15 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/31 10:20:15 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "parser.h"
#include "compiler.h"
#include "libft.h"
#include "dynarray.h"

static char		*g_levels[LOG_MAX] = {
	[LOG_DEBUG] = "\e[1;34mdebug\e[2m:\e[0m",
	[LOG_SUCCESS] = "\e[1;32msuccess\e[2m:\e[0m",
	[LOG_INFO] = "\e[1;34minfo\e[2m:\e[0m",
	[LOG_WARN] = "\e[1;35mwarning\e[2m:\e[0m",
	[LOG_CRIT] = "\e[1;31merror\e[2m:\e[0m",
};

static inline void		get_print_path(
		t_dynarray *str,
		t_print_compiler data
)
{
	ft_dynarray_push_str(str, "\e[1m");
	if (data.file_path)
		ft_dynarray_push_str(str, data.file_path);
	else
		ft_dynarray_push_str(str, "???");
	ft_dynarray_push(str, ":\e[0;1m ", 7 + !!data.no_print_location);
}

static inline void		get_print_location(
		t_dynarray *str,
		t_print_compiler data
)
{
	if (data.target)
	{
		ft_dynarray_push_str(str, ft_ultostr(data.target->line, 10, 0));
		ft_dynarray_push(str, ":", 1);
		ft_dynarray_push_str(str, ft_ultostr(data.target->col, 10, 0));
		ft_dynarray_push(str, ": ", 2);
	}
	else
	{
		ft_dynarray_push_str(str, ft_ultostr(data.line, 10, 0));
		ft_dynarray_push(str, ":", 1);
		ft_dynarray_push_str(str, ft_ultostr(data.col, 10, 0));
		ft_dynarray_push(str, ": ", 2);
	}
}

static inline void		get_print_output(
		t_dynarray *str,
		t_print_compiler data
)
{
	if (data.message)
	{
		ft_dynarray_push_str(str, data.message);
		ft_dynarray_push(str, "\n", 1);
	}
	else
		ft_dynarray_push_str(str, "Unexpected error occured.\n");
	ft_dynarray_push(str, "\e[0m", 4);
}

void					compiler_printer_free(void *data)
{
	free(((t_print_compiler*)data)->message);
}

void					compiler_printer(t_print print)
{
	t_dynarray			str;
	t_print_compiler	data;

	str = ft_dynarray_create_loc(0, 0);
	data = *(t_print_compiler*)print.data;
	if (!data.no_print_file)
		get_print_path(&str, data);
	if (!data.no_print_location)
		get_print_location(&str, data);
	if (!data.no_print_header && print.level < LOG_MAX && print.level > 0)
	{
		ft_dynarray_push_str(&str, g_levels[print.level]);
		ft_dynarray_push(&str, " ", 1);
	}
	get_print_output(&str, data);
	// If there is a target to print
	if (data.target)
	{
		// Handles conflict token
		if (data.conflict && data.conflict->line < data.target->line)
		{
			get_error_location(&str, data.conflict, data, print.level);
			data.scope_before += data.target->line - data.conflict->line - 1;
			if (data.scope_before > 99)
				data.scope_before = 99;
		}
		else if (data.conflict)
		{
			data.scope_after += data.conflict->line - data.target->line - 1;
			if (data.scope_after > 99)
				data.scope_after = 99;
		}
		if (data.target && data.file)
			get_error_location(&str, data.target, data, print.level);
		if (data.conflict && data.conflict->line >= data.target->line)
			get_error_location(&str, data.conflict, data, print.level);
	}
	write(2, str.array + str.offset, str.index);
	ft_dynarray_destroy(&str, false);
}
