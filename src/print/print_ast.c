/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_ast.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/19 04:28:39 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/31 14:13:32 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ast.h"
#include "dynarray.h"

void					print_ast(t_ast *ast)
{
	t_ast_node			*node;
	uint64_t			i;

	i = 0;
	printf("Ast: [\n");
	while ((node = ft_dynarray_iterate(&ast->tree, &i, sizeof(t_ast_node))))
	{
		print_ast_node_tree_wrap(node, 1);
	}
	printf("]\n");
}
