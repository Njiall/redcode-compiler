/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_error_location.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/04 06:16:36 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/31 10:21:53 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/ioctl.h>
#include <stdarg.h>
#include <stdlib.h>
#include "print.h"
#include "libft.h"
#include "compiler.h"
#include <stdio.h>
#include <math.h>

/*
** Returns a newly allocated string without the tab char with configured tabstop
*/
static inline char			*_get_formated_line(
		char *str,
		size_t len,
		size_t display_len
)
{
	char					*fmt;
	size_t					j;
	if (!display_len)
		return (ft_strdup(""));
	if (!(fmt = malloc(display_len)))
		return (NULL);
	j = 0;
	for (size_t i = 0; i < len; i++)
	{
		if (str[i] == '\t')
		{
			size_t shift = TABSTOP - (j % TABSTOP);
			/* ft_memcpy(fmt + j, "+-------", shift); */
			ft_memcpy(fmt + j, "        ", shift);
			j += shift;
		}
		else
		{
			fmt[j] = str[i];
			j++;
		}
	}
	return (fmt);
}

static inline void			_print_before_lines(
		t_dynarray *str,
		t_win_info info
)
{
	size_t					len;


	for (int i = 0; i < info.before_count; i++)
	{
		ft_dynarray_push(str, "          ", info.line_count_offset - 3
				- (uint32_t)log10(info.before_count - i));
		ft_dynarray_push_str(str, "\e[1;33m-");
		ft_dynarray_push_str(str, ft_ultostr(info.before_count - i, 10, 1));
		ft_dynarray_push_str(str, "\e[0m \e[2m|\e[0;2m");
		if (info.before_lines[i].l_overflow)
			ft_dynarray_push_str(str, "\e[2m...\e[0m");
		if (info.before_lines[i].len > info.offset)
		{
			if (info.before_lines[i].len - info.offset
					> info.cols - info.line_count_offset - 1
					- 3 * info.before_lines[i].l_overflow
					- 3 * info.before_lines[i].r_overflow)
				len = info.cols - info.line_count_offset - 1
					- 3 * info.before_lines[i].l_overflow
					- 3 * info.before_lines[i].r_overflow;
			else
				len = info.before_lines[i].len - info.offset;
			ft_dynarray_push(str, info.before_lines[i].start + info.offset, len);
			free(info.before_lines[i].start);
		}
		if (info.before_lines[i].r_overflow)
			ft_dynarray_push_str(str, "\e[2m...\e[0m");
		ft_dynarray_push_str(str, "\e[0m\n");
	}
}

static inline void			_print_after_lines(
		t_dynarray *str,
		t_win_info info
)
{
	size_t					len;

	for (int i = 0; i < info.after_count; i++)
	{
		ft_dynarray_push(str, "          ", info.line_count_offset - 3
				- (uint32_t)log10(info.after_count - i));
		ft_dynarray_push_str(str, "\e[1;32m+");
		ft_dynarray_push_str(str, ft_ultostr(i + 1, 10, 1));
		ft_dynarray_push_str(str, "\e[0m \e[2m|\e[0m");
		if (info.after_lines[i].l_overflow)
			ft_dynarray_push_str(str, "\e[2m...\e[0m");
		if (info.after_lines[i].len > info.offset)
		{
			if (info.after_lines[i].len - info.offset
					> info.cols - info.line_count_offset - 1
					- 3 * info.after_lines[i].l_overflow
					- 3 * info.after_lines[i].r_overflow)
				len = info.cols - info.line_count_offset - 1
					- 3 * info.after_lines[i].l_overflow
					- 3 * info.after_lines[i].r_overflow;
			else
				len = info.after_lines[i].len - info.offset;
			ft_dynarray_push(str, info.after_lines[i].start + info.offset, len);
			free(info.after_lines[i].start);
		}
		if (info.after_lines[i].r_overflow)
			ft_dynarray_push_str(str, "\e[2m...\e[0m");
		ft_dynarray_push(str, "\n", 1);
	}
}

static inline void			_print_suggest(
		t_dynarray *str,
		t_win_info info,
		const t_token *target,
		t_print_level level,
		char *suggest,
		int suggest_offset

)
{
	ft_dynarray_push(str, "         ", info.line_count_offset - 1);
	ft_dynarray_push_str(str, (level == LOG_WARN)
			? "\e[0m \e[2m|\e[0;1;35m" : "\e[0m \e[2m|\e[0;1;31m");
	if (info.target_line.l_overflow)
		ft_dynarray_push_str(str, (level == LOG_WARN)
				? "\e[2m...\e[0;1;35m" : "\e[2m...\e[0;1;31m");

	// Append spaces until target alignement
	size_t i = info.offset + 1;
	char *s = info.target_line.start + info.offset;
	for (;*s && i < target->col - target->loffset + suggest_offset; i++)
		ft_dynarray_push(str, " ", 1);
	ft_dynarray_push_str(str, suggest);
	ft_dynarray_push_str(str, "\e[0m\n");
	free(suggest);
}

static inline void			_print_target_underline(
		t_dynarray *str,
		t_win_info info,
		const t_token *target,
		t_print_level level,
		char *suggest,
		int suggest_offset
)
{
	ft_dynarray_push(str, "         ", info.line_count_offset - 1);
	ft_dynarray_push_str(str, (level == LOG_WARN)
			? "\e[0m \e[2m|\e[0;1;35m" : "\e[0m \e[2m|\e[0;1;31m");
	if (info.target_line.l_overflow)
		ft_dynarray_push_str(str, (level == LOG_WARN)
				? "\e[2m...\e[0;1;35m" : "\e[2m...\e[0;1;31m");

	// Append spaces until target alignement
	size_t i = info.offset + 1;
	char *s = info.target_line.start + info.offset;
	for (;*s && i < target->col - target->loffset; i++)
		ft_dynarray_push(str, " ", 1);
	// Append underline
	for (; i < target->col; i++)
		ft_dynarray_push(str, "~", 1);
	for (; i < target->col + target->len
			&& i - info.offset < info.cols - info.line_count_offset
			- 3 * info.target_line.l_overflow
			- 3 * info.target_line.r_overflow; i++)
		ft_dynarray_push(str, "^", 1);
	for (; i < target->col + target->roffset + target->len
			&& i - info.offset < info.cols - info.line_count_offset
			- 3 * info.target_line.l_overflow
			- 3 * info.target_line.r_overflow; i++)
		ft_dynarray_push(str, "~", 1);
	if (info.target_line.r_overflow)
		ft_dynarray_push_str(str, "\e[2m...");
	ft_dynarray_push_str(str, "\e[0m\n");
	if (suggest)
		_print_suggest(str, info, target, level, suggest, suggest_offset);
	free(info.target_line.start);
}

static inline void			_print_target_line(
		t_dynarray *str,
		t_win_info info,
		const t_token *target
)
{
	size_t					len;
	char					*line;

	if (info.before_count)
	{
		ft_dynarray_push(str, "         ", info.line_count_offset - 1);
		ft_dynarray_push_str(str, "\e[0m \e[2m|\e[0;1;32m");
		ft_dynarray_push(str, "\n", 1);
	}
	line = ft_ultostr(target->line, 10, 1);
	if ((len = ft_strlen(line)) < 4)
		ft_dynarray_push(str, "      ", 3 - len);
	ft_dynarray_push_str(str, "\e[1;4;34m");
	ft_dynarray_push_str(str, line);
	ft_dynarray_push_str(str, "\e[0m \e[2m|\e[0m");
	if (info.target_line.l_overflow)
		ft_dynarray_push_str(str, "\e[2m...\e[0m");
	if (info.target_line.len - info.offset
			> info.cols - info.line_count_offset - 1
			- 3 * info.target_line.l_overflow
			- 3 * info.target_line.r_overflow)
		len = info.cols - info.line_count_offset - 1
			- 3 * info.target_line.l_overflow
			- 3 * info.target_line.r_overflow;
	else
		len = info.target_line.len - info.offset;
	ft_dynarray_push(str, info.target_line.start + info.offset, len);
	if (info.target_line.r_overflow)
		ft_dynarray_push_str(str, "\e[2m...\e[0m");
	ft_dynarray_push(str, "\n", 1);
}

// Asumes that the string exists up to target.len
// before_target [0, 5]
// after_target  [0, 5]
static inline t_win_info	_get_display_info(
		const t_token *target,
		const t_token *conflict,
		const char *file,
		size_t before_target,
		size_t after_target
)
{
	struct winsize			win;
	t_win_info				info;
	size_t					i;
	size_t					j;
	size_t					escape_len;

	// Get terminal width
	ioctl(2, TIOCGWINSZ, &win);
	info = (t_win_info){.before_count = 0};
	info.cols = win.ws_col;
	// Get offset for line number
	if ((info.line_count_offset = (size_t)log10(target->line)) > 10)
		info.line_count_offset = 10;
	else if (info.line_count_offset < 4)
		info.line_count_offset = 4;

	// Calculates offset to know where to show
	if (target->col - target->loffset > (info.cols / 8) * 7 - 10
			&& (info.target_line.l_overflow = true))
		info.offset = target->col - ((info.cols / 5) * 2 - 10 - target->len / 2);

	// Get file line corresponding to target token
	// 		with added lines for before and after target
	//
	// Jumps to the code start
	char *str = (char*)file;
	i = 1;
	for (; *str && i + before_target < target->line; str++)
		if (*str == '\n')
			i++;

	j = 0;
	escape_len = 0;
	// Before lines loop
	char *start = str;
	for (; *str && i < target->line; str++)
		if (*str == '\t')
		{
			j += TABSTOP - (j % TABSTOP);
			escape_len += 8;
		}
		else if (*str == '\n')
		{
			info.before_lines[info.before_count++] = (t_line_info){
				.start = _get_formated_line(start, str - start, j),
				.len = j
			};
			if (str - start && info.target_line.l_overflow)
				info.before_lines[info.before_count - 1].l_overflow = true;
			if (j > info.cols - info.line_count_offset - 1 + info.offset)
				info.before_lines[info.before_count - 1].r_overflow = true;
			start = str + 1;
			escape_len = 0;
			j = 0;
			i++;
		}
		else
			j++;
	j = 0;
	escape_len = 0;
	// Targeted line
	for (; *str && i < target->line + 1; str++)
	{
		if (*str == '\t')
		{
			j += TABSTOP - (j % TABSTOP);
			escape_len += 8;
		}
		else if (*str == '\n' || !*str)
		{
			info.target_line = (t_line_info){
				.start = _get_formated_line(start, str - start, j),
				.len = j, .l_overflow = info.target_line.l_overflow
			};
			if (j - info.offset > info.cols - info.line_count_offset - 1)
				info.target_line.r_overflow = true;
			i++;
		}
		else
			j++;
	}
	j = 0;
	escape_len = 0;
	// After lines loop
	start = str;
	for (; *str && i < target->line + after_target + 1; str++)
		if (*str == '\t')
		{
			j += TABSTOP - (j % TABSTOP);
			escape_len += 8;
		}
		else if (*str == '\n' || !*str)
		{
			info.after_lines[info.after_count++] = (t_line_info){
				.start = _get_formated_line(start, str - start, j),
				.len = j
			};
			if (str - start && info.target_line.l_overflow)
				info.after_lines[info.after_count - 1].l_overflow = true;
			if (j > info.cols - info.line_count_offset - 1 + info.offset)
				info.after_lines[info.after_count - 1].r_overflow = true;
			start = str + 1;
			escape_len = 0;
			j = 0;
			i++;
		}
		else
			j++;

	return (info);
}

void					get_error_location(
		t_dynarray *str,
		t_token *target,
		t_print_compiler data,
		t_print_level level
)
{
	t_win_info			info;

	info = _get_display_info(target, data.conflict, data.file, data.scope_before, data.scope_after);
	_print_before_lines(str, info);
	_print_target_line(str, info, target);
	_print_target_underline(str, info, target, level, data.suggest, data.suggest_offset);
	_print_after_lines(str, info);
}
