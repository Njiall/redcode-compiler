/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/24 19:07:04 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/07 14:42:29 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "print.h"
#include "libft.h"

void							printer(t_print print)
{
	static const char *const	headers[LOG_MAX] = {
		[LOG_DEBUG] = LOG_DEBUG_STR,
		[LOG_SUCCESS] = LOG_SUCCESS_STR,
		[LOG_INFO] = LOG_INFO_STR,
		[LOG_WARN] = LOG_WARN_STR,
		[LOG_CRIT] = LOG_CRIT_STR,
	};
	char						*msg;

	if (print.flags.silent)
		return ;
	if (!print.flags.no_header)
	{
		msg = ft_strajoin(2, headers[print.level], print.data);
		write(1, msg, ft_strlen(msg));
		free(msg);
	}
	else
		write(1, print.data, ft_strlen(print.data));
}

void							print(t_print print)
{
	if (print.printer && !print.flags.silent
			&& !(!print.flags.debug && print.level == LOG_DEBUG))
		print.printer(print);
	if (print.data && print.destructor)
		print.destructor(print.data);
	if ((print.level == LOG_CRIT || print.flags.exit
			|| (print.flags.strict && print.level == LOG_WARN))
			&& !print.flags.no_exit)
		exit(print.code);
}
