# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/04 04:17:21 by mbeilles          #+#    #+#              #
#    Updated: 2019/10/31 14:19:02 by mbeilles         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#==============================================================================#
#                                  VARIABLES                                   #
#------------------------------------------------------------------------------#
#                                Customisation                                 #
#==============================================================================#

NAME = compiler

NICK = CPL
PROJECT_COLOR = "\033[38;5;32m"
PROJECT_COLOR_ALT = "\033[38;5;33m"

CC = clang

#==============================================================================#
#                                   Sources                                    #
#==============================================================================#

SRCS =	$(PATH_SRC)/main.c													\
\
		$(PATH_SRC)/tokens/tokenizer.c										\
		$(PATH_SRC)/tokens/registers.c										\
\
		$(PATH_SRC)/ast/construct.c											\
		$(PATH_SRC)/ast/construct_instruction.c								\
		$(PATH_SRC)/ast/construct_property.c								\
		$(PATH_SRC)/ast/construct_expr.c									\
		$(PATH_SRC)/ast/destroy.c											\
\
		$(PATH_SRC)/ast/levenshtein.c										\
\
		$(PATH_SRC)/print/print_tokens.c									\
		$(PATH_SRC)/print/print_token.c										\
\
		$(PATH_SRC)/print/print_ast.c										\
		$(PATH_SRC)/print/print_ast_node_tree.c								\
\
		$(PATH_SRC)/print/print.c											\
		$(PATH_SRC)/print/print_compiler.c									\
		$(PATH_SRC)/print/print_error_location.c							\
		$(PATH_SRC)/print/print_parser_output.c								\


INCS = $(PATH_INC)/corewar.h												\

LIBS =	\
		./libft \
		./libbboa \

SYSLIBS =	 \

SYSLIBS_LINUX =	\

TESTS =	\

#==============================================================================#
#                                   Paths                                      #
#==============================================================================#

PATH_SRC := src
PATH_INC := include
PATH_OBJ := .obj
PATH_DEP := .dep

#==============================================================================#
#                                 Compilation                                  #
#==============================================================================#

LDLIBS = \

CLIBS = $(foreach dep, $(LIBS), $(dep)/$(notdir $(dep)).a) \

LDFLAGS =
LDFLAGS_DARWIN =
LDFLAGS_LINUX = -fuse-ld=lld # Don't forget to install lld as ld don't use .a files

CFLAGS = -I$(PATH_INC) \
		 $(foreach dep, $(LIBS), -I$(dep)/$(PATH_INC)) \
		 $(foreach dep, $(LDLIBS), -I$(dep)/$(PATH_INC)) \

#==============================================================================#
#                                   Various                                    #
#==============================================================================#

SHELL = bash

#==============================================================================#
#                             Variables Customizers                            #
#==============================================================================#

OBJS := $(patsubst %.c, $(PATH_OBJ)/%.o, $(SRCS))
DEPS := $(patsubst %.c, $(PATH_DEP)/%.d, $(SRCS))

#==============================================================================#
#                                    Rules                                     #
#==============================================================================#

include makefiles/bin.mk
include makefiles/test.mk
include makefiles/strings.mk
include makefiles/depend.mk
