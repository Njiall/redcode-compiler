/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: njiall <mbeilles@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/30 18:31:43 by njiall            #+#    #+#             */
/*   Updated: 2019/10/19 10:43:10 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_H
# define PARSER_H

# include "compiler.h"
# include "dynarray.h"
# include "hashmap.h"

typedef enum		e_tok_state {
	PARSER_DEFAULT_ERR,
	// Non critical
	PARSER_ERR_NSP_A1,
	PARSER_ERR_NSP_A2,
	PARSER_ERR_NSP_A3,
	PARSER_ERR_WRONG_TYPE_NC,
	// Critical
	PARSER_ERR_UNDEFINED_OP,
	PARSER_ERR_WRONG_REG,
	PARSER_ERR_NOT_ENOUGH_ARGS,
	PARSER_ERR_TOO_MANY_ARGS,
	PARSER_ERR_WRONG_TYPE_C,
	PARSER_MAX_ERR,
	// Normal parsing states
	PARSER_INIT,
	// Instructions
	// 		Arguments
	PARSER_OP_START,
	PARSER_OP_ARG_1,
	PARSER_OP_ARG_2,
	PARSER_OP_ARG_3,
	PARSER_OP_ARG_4,
	PARSER_OP_ARG_1_S,
	PARSER_OP_ARG_2_S,
	PARSER_OP_ARG_3_S,
	PARSER_EXPR,
	// Miscs
	PARSER_KEY,
	PARSER_EXEC,
	PARSER_SKIP,
	// Used for arrays
	PARSER_MAX,
}					t_tok_state;

# define PARSER_ANY		PARSER_DEFAULT_ERR ... PARSER_MAX - 1

# define ERR_CRIT		PARSER_ERR_UNDEFINED_OP ... PARSER_MAX_ERR - 1
# define ERR_WARN		PARSER_DEFAULT_ERR + 1 ... PARSER_ERR_UNDEFINED_OP - 1

typedef enum		e_op_type {
	OP_NONE,
	OP_NOPE,
	OP_LIVE,

	OP_ST,
	OP_LD,

	OP_ADD,
	OP_SUB,

	OP_AND,
	OP_OR,
	OP_XOR,

	OP_MAX,
}					t_op_type;

# define OP_LOGIC	OP_ADD ... OP_SUB
# define OP_BOOL	OP_AND ... OP_XOR

typedef struct		s_parse_state_machine {
	t_state			state;
	t_tok_state		prev_state;
	t_tok_state		next_state;
	t_op_type		type;
	t_token			*token;
	t_token			*op_token;
	char			*file;
}					t_parse_state_machine;

t_state				parse(t_dynarray *tokens, t_binary *binary);

// Sub parsers
t_state				parse_instruction(t_dynarray *tokens, t_token *token, char *file_path, char *file, uint64_t *index);
t_state				parse_property(t_dynarray *tokens, t_token *token, char *file_path, char *file, uint64_t *index);
t_state				parse_key(t_dynarray *tokens, t_token *token, char *file_path, char *file, uint64_t *index);

#endif
