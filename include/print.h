/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/24 19:00:17 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/07 14:41:55 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRINT_H
# define PRINT_H

# include <stdbool.h>

# define LOG_DEBUG_STR			"\e[0;37m[\e[1;32m?\e[0;37m]\e[0m "
# define LOG_SUCCESS_STR		"\e[0;37m[\e[1;32m+\e[0;37m]\e[0m "
# define LOG_INFO_STR			"\e[0;37m[\e[1;34mi\e[0;37m]\e[0m "
# define LOG_WARN_STR			"\e[0;37m[\e[1;33m!\e[0;37m]\e[0m "
# define LOG_CRIT_STR			"\e[0;37m[\e[1;31mx\e[0;37m]\e[0m "

typedef enum			e_print_level
{
	LOG_DEBUG,
	LOG_SUCCESS,
	LOG_INFO,
	LOG_WARN,
	LOG_CRIT,
	LOG_MAX,
	LOG_SAFE_RANGE
}						t_print_level;

typedef enum			e_print_code
{
	ERR_NONE,
	ERR_GENERIC,
	ERR_ALLOC,
	ERR_NO_SPACE,
	ERR_MAX,
	ERR_SAFE_RANGE
}						t_print_code;

typedef struct			s_print_flags
{
	bool				exit : 1;
	bool				no_exit : 1;
	bool				silent : 1;
	bool				strict : 1;
	bool				no_header : 1;
	bool				debug : 1;
}						t_print_flags;

typedef struct			s_print
{
	t_print_code		code;
	t_print_level		level;
	t_print_flags		flags;
	void				*data;
	void				(*printer)(struct s_print);
	void				(*destructor)(void *);
}						t_print;

void					print(t_print print);
void					printer(t_print print);
# define PRINT_DESTROY	(&free)

#endif
