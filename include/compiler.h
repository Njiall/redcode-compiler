/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compiler.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/28 14:37:20 by mbeilles          #+#    #+#             */
/*   Updated: 2019/11/02 17:31:29 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMPILER_H
# define COMPILER_H

# include <stdbool.h>
# include <stddef.h>
# include <sys/ioctl.h>

# include "../libft/include/dynarray.h"
# include "../libft/include/hashmap.h"
# include "print.h"
# include "registers.h"

# define TABSTOP		4

# define TOK_STR_WORD			"a \e[1;30;4mword\e[0m"
# define TOK_STR_NUMBER			"a \e[1;30;4mnumber\e[0m"
# define TOK_STR_DIRECT_NUMBER	"a \e[1;30;4mdirect number\e[0m"
# define TOK_STR_NEWLINE		"a \e[1;30;4mnewline\e[0m"
# define TOK_STR_SEPARATOR		"a \e[1;30;4mseparator\e[0m"
# define TOK_STR_INDIRECT_REF	"an \e[1;30;4mindirect reference\e[0m"
# define TOK_STR_DIRECT_REF		"a \e[1;30;4mdirect reference\e[0m"
# define TOK_STR_KEY_REF		"a \e[1;30;4mkey\e[0m"
# define TOK_STR_PROP			"a \e[1;30;4mproperty\e[0m"
# define TOK_STR_STRING			"a \e[1;30;4mstring\e[0m"
# define TOK_STR_REGISTER		"a \e[1;30;4mregister\e[0m"

# define TOK_STR_ADD			"a \e[1;30;4m+\e[0m"
# define TOK_STR_SUB			"a \e[1;30;4m-\e[0m"
# define TOK_STR_MULT			"a \e[1;30;4m*\e[0m"
# define TOK_STR_DIV			"a \e[1;30;4m/\e[0m"

# define TOK_STR_AND_LOG		"a \e[1;30;4m&&\e[0m"
# define TOK_STR_XOR_LOG		"a \e[1;30;4m^^\e[0m"
# define TOK_STR_OR_LOG			"a \e[1;30;4m||\e[0m"
# define TOK_STR_AND_BIT		"a \e[1;30;4m&\e[0m"
# define TOK_STR_XOR_BIT		"a \e[1;30;4m^\e[0m"
# define TOK_STR_OR_BIT			"a \e[1;30;4m|\e[0m"

# define TOK_STR_LSHIFT			"a \e[1;30;4m<<\e[0m"
# define TOK_STR_RSHIFT			"a \e[1;30;4m>>\e[0m"

# define TOK_STR_NOT			"a \e[1;30;4m!\e[0m"
# define TOK_STR_EXPR_BRC_OPEN	"a \e[1;30;4m(\e[0m"
# define TOK_STR_EXPR_BRC_CLOSE	"a \e[1;30;4m)\e[0m"

# define TOK_STR_DIRECT			"a \e[1;30;4mdirect value\e[0m"
# define TOK_STR_INDIRECT		"an \e[1;30;4mindirect value\e[0m"

typedef struct			s_state {
	bool				error;
	size_t				errors;
	size_t				warnings;
	size_t				line;
	size_t				col;
}						t_state;

/*
** =============================================================================
** 						Registers
** =============================================================================
*/

typedef enum			e_reg_id {
	RID_NONE,
	RID_1,
	RID_2,
	RID_3,
	RID_4,
	RID_5,
	RID_6,
	RID_7,
	RID_8,
	RID_9,
	RID_10,
	RID_11,
	RID_12,
	RID_13,
	RID_14,
	RID_15,
	RID_16,
	RID_MAX
}						t_reg_id;

typedef enum		e_state_reg {
	REG_ERR,
	REG_R1 = RID_1,
	REG_R2,
	REG_R3,
	REG_R4,
	REG_R5,
	REG_R6,
	REG_R7,
	REG_R8,
	REG_R9,
	REG_R10,
	REG_R11,
	REG_R12,
	REG_R13,
	REG_R14,
	REG_R15,
	REG_R16,
	REG_INIT,
	REG_R,
	REG_R0,
	REG_MAX
}					t_state_reg;

/*
** =============================================================================
** 						Token types
** =============================================================================
*/

typedef enum			e_token_type {
	TOK_NONE,

	TOK_WORD, //     Generic word syntax: '[a-zA-Z0-9_]+'
	TOK_NUMBER, // Generic number syntax: '(-|+|)[0-9]+'

	TOK_NEWLINE, //       Newline symbol: '\n'
	TOK_SEPARATOR, //   Separator symbol: ','

	TOK_REGISTER, //              Syntax: 'r([1-9]|1[0-6])'
	TOK_DIRECT_NUMBER, //         Syntax: '%[:number:]'
	TOK_INDIRECT_REF, //          Syntax: ':[:word:]'
	TOK_DIRECT_REF, //            Syntax: '%:[:word:]'
	TOK_KEY_REF, //               Syntax: '[:word:]:'

	TOK_OP_ADD, //                Syntax: '+'
	TOK_OP_RNG_START = TOK_OP_ADD,
	TOK_OP_SUB, //                Syntax: '-'
	TOK_OP_DIV, //                Syntax: '/'
	TOK_OP_MULT, //               Syntax: '*'
	TOK_OP_AND_BIT, //            Syntax: '&'
	TOK_OP_XOR_BIT, //            Syntax: '^'
	TOK_OP_OR_BIT, //             Syntax: '|'
	TOK_OP_AND_LOG, //            Syntax: '&&'
	TOK_OP_XOR_LOG, //            Syntax: '^^'
	TOK_OP_OR_LOG, //             Syntax: '||'
	TOK_OP_LSHIFT, //             Syntax: '<<'
	TOK_OP_RSHIFT, //             Syntax: '>>'
	TOK_OP_NOT, //                Syntax: '!'
	TOK_EXPR_BRC_OPEN, //         Syntax: '('
	TOK_EXPR_BRC_CLOSE, //        Syntax: ')'
	TOK_OP_RNG_END = TOK_EXPR_BRC_CLOSE,

	TOK_PROP, //                  Synatx: .[:word:](=| )
	TOK_STRING, //                Synatx: ["'].*["']

	TOK_MAX
}						t_token_type;

# define TOK_ANY		TOK_NONE ... TOK_MAX - 1
# define TOK_ANY_EXPR	TOK_OP_RNG_START ... TOK_OP_RNG_END

/*
** =============================================================================
** 						Token structs
** =============================================================================
*/

struct					s_token_void {
	t_token_type		type;
	size_t				line;
	size_t				col;
	size_t				loffset;
	size_t				roffset;
	size_t				len;
	char				*str;
};

struct					s_token_word {
	t_token_type		type;
	size_t				line;
	size_t				col;
	size_t				loffset;
	size_t				roffset;
	size_t				len;
	char				*str;
};

struct					s_token_number {
	t_token_type		type;
	size_t				line;
	size_t				col;
	size_t				loffset;
	size_t				roffset;
	size_t				len;
	char				*str;
	int32_t				num;
};

typedef struct			s_token_register {
	t_token_type		type;
	size_t				line;
	size_t				col;
	size_t				loffset;
	size_t				roffset;
	size_t				len;
	char				*str;
	t_reg_id			id;
}						t_token_register;

/*
** =============================================================================
** 						Token aliases
** =============================================================================
*/

typedef struct			s_token_word t_token_word;
typedef struct			s_token_number t_token_num;
typedef struct			s_token_word t_token_label;
typedef struct			s_token_void t_token_newline;
typedef struct			s_token_void t_token_separator;
typedef struct			s_token_void t_token_eof;

/*
** =============================================================================
** 						Token
** =============================================================================
*/

typedef union			u_token {
	struct {
		t_token_type	type;
		size_t			line;
		size_t			col;
		size_t			loffset;
		size_t			roffset;
		size_t			len;
		char			*str;
	};
	t_token_word		word;
	t_token_num			number;
	t_token_label		label;
	t_token_newline		newline;
	t_token_separator	separator;
	t_token_eof			eof;
	t_token_register	reg;
}						t_token;

/*
** =============================================================================
** 						State-machine structs
** =============================================================================
*/

typedef enum			e_char_class {
	CH_NONE,
	CH_SEPARATOR, //   ','
	CH_SPACE, //       ' '
	CH_WORD, //        '[a-zA-Z_]'
	CH_NUMBER, //      '[0-9]'
	CH_NEWLINE, //     '\n'
	CH_LABEL, //       ':'
	CH_DIRECT, //      '%'
	CH_COMMENT, //     '[#;]'

	CH_PLUS, //        '+'
	CH_MINUS, //       '-'
	CH_SLASH, //       '/'
	CH_STAR, //        '*'
	CH_AMP, //         '&'
	CH_CARET, //       '^'
	CH_PIPE, //        '|'
	CH_RCHEV, //       '>'
	CH_LCHEV, //       '<'
	CH_BANG, //        '!'

	CH_BRACE_OPEN, //  '('
	CH_BRACE_CLOSE, // ')'

	CH_EOF, //         '\0'

	CH_DOT, //         '.'
	CH_QUOTE, //       '''
	CH_DQUOTE, //      '"'

	CH_MAX,
}						t_char_class;

typedef enum			e_word_state {
	// Default states
	ST_ERROR,
	ST_INIT,

	// Lambda Token generation
	ST_WORD,
	ST_NUMBER,
	ST_DIRECT_NUMBER,
	ST_INDIRECT_LABEL,
	ST_DIRECT_LABEL,
	ST_LABEL_KEY,
	ST_SPACE,
	ST_DIRECT,
	ST_PROP,
	ST_PROP_NAME,

	// Delimiters
	ST_NEWLINE,
	ST_SEPARATOR,

	// Binary operators tokens
	ST_PLUS,
	ST_MINUS,
	ST_MULT,
	ST_DIVIDE,

	ST_AND_BIT,
	ST_XOR_BIT,
	ST_OR_BIT,
	ST_AND_LOG,
	ST_XOR_LOG,
	ST_OR_LOG,

	ST_RCHEV,
	ST_LCHEV,
	ST_RSHIFT,
	ST_LSHIFT,
	// Unary operators
	ST_NOT,
	ST_BRACE_OPEN,
	ST_BRACE_CLOSE,

	ST_EOF,
	ST_INVALID_END,

	// Special cases
	ST_COMMENT,
	ST_STR_D,
	ST_STR_S,

	// Used only to delimit arrays
	ST_MAX
}						t_word_state;

# define ST_INCONDITIONAL	ST_INIT ... ST_EOF
# define ST_OPERATOR		ST_PLUS ... ST_BRACE_CLOSE

/*
** =============================================================================
** 						Binary
** =============================================================================
** 				Binary creation
** =============================================================================
*/

# define COR_FILE_MAGIC		0x00ea83f3

typedef struct			s_file_header {
	uint32_t			magic;
	char				name[128];
	uint32_t			padding_1;
	char				comment[2048];
	uint32_t			padding_2;
	uint32_t			size;
}						t_file_header;

typedef struct			s_binary {
	t_token				*name_token;
	t_token				*comment_token;

	t_dynarray			code;
	char				*output_path;
	char				*output_content;

	char				*source_path;
	char				*source_content;
}						t_binary;

/*
** =============================================================================
** 				Lexer
** =============================================================================
*/

t_state					tokenize(t_dynarray *tokens, t_binary *binary);

/*
** =============================================================================
** 				Token conversion subparser
** =============================================================================
*/

t_state					convert_to_register(t_token *word);

/*
** =============================================================================
** 						Suggestion algorithm
** =============================================================================
*/

struct					s_sl_list
{
	char	*str;
	size_t	len;
};

typedef struct			s_string_list {
	struct s_sl_list	list[50];
	size_t				len;
}						t_string_list;

char					*get_suggestion(
		char *input,
		size_t len,
		t_string_list data
);

/*
** =============================================================================
** 						Printing
** =============================================================================
** 				Default printing
** =============================================================================
*/

void					print_tokens(t_dynarray *tokens);
void					print_token(t_token *token);

/*
** =============================================================================
** 				Specific printing
** =============================================================================
*/

typedef struct			s_print_compiler {
	uint64_t			line;
	uint64_t			col;

	t_token				*target;
	t_token				*conflict;
	char				*file_path;
	char				*file;
	char				*message;
	char				*suggest;
	size_t				scope_after;
	size_t				scope_before;
	int					suggest_offset;

	bool				no_print_file: 1;
	bool				no_print_location: 1;
	bool				no_print_code: 1;
	bool				no_print_header: 1;
	bool				no_print_message: 1;
}						t_print_compiler;

typedef struct			s_print_pattern
{
	char				*pattern;
	char				*replace;
	size_t				plen;
	size_t				rlen;
}						t_print_pattern;

typedef struct			s_line_info
{
	char				*start;
	size_t				len;
	size_t				display_len;
	bool				r_overflow : 1;
	bool				l_overflow : 1;
}						t_line_info;

typedef struct			s_win_info
{
	t_line_info			target_line;
	t_line_info			before_lines[99];
	t_line_info			after_lines[99];
	size_t				before_count;
	size_t				after_count;

	size_t				line_count_offset;
	size_t				cols;

	size_t				offset;
}						t_win_info;

void					compiler_printer(t_print print);
void					compiler_printer_free(void *data);

char					*replace_format_placeholders(
		char *error_message,
		int number,
		...
);

void					get_error_location(
		t_dynarray *str,
		t_token *target,
		t_print_compiler data,
		t_print_level level
);

char					*get_compiler_print_format(
		t_print_level level,
		t_token *target,
		char *file_path,
		char *message
);

#endif
