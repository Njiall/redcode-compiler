/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/13 19:33:07 by mbeilles          #+#    #+#             */
/*   Updated: 2019/11/02 19:41:02 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AST_H
# define AST_H

# include "compiler.h"

/*
** =============================================================================
** 				Op structures
** =============================================================================
*/
typedef enum			e_expr_op_type
{
	EXPR_OP_SUB,
	EXPR_OP_ADD,
	EXPR_OP_MULT,
	EXPR_OP_DIV,

	EXPR_OP_AND_LOG,
	EXPR_OP_XOR_LOG,
	EXPR_OP_OR_LOG,
	EXPR_OP_AND_BIT,
	EXPR_OP_XOR_BIT,
	EXPR_OP_OR_BIT,

	EXPR_OP_RSHIFT,
	EXPR_OP_LSHIFT,

	EXPR_OP_NOT,
	EXPR_OP_MAX
}						t_expr_op_type;

typedef enum			e_expr_arg_type
{
	EXPR_TYPE_NONE,
	EXPR_TYPE_OP,
	EXPR_TYPE_TOKEN,
	EXPR_TYPE_NUMBER,
	EXPR_TYPE_LONG_NUMBER,
	EXPR_TYPE_EXPR,
	EXPR_TYPE_MAX
}						t_expr_arg_type;

/*
** =============================================================================
** 				Expressions substructures
** =============================================================================
*/

typedef struct			s_expr_token
{
	t_expr_arg_type		type;
	t_token				token;
	union u_expr		*args;
	uint8_t				len;
	t_token_type		result_type;
}						t_expr_token;

typedef struct			s_expr_number
{
	t_expr_arg_type		type;
	t_token				token;
	union u_expr		*args;
	uint8_t				len;
	t_token_type		result_type;

	// Specifics
	int32_t				num;
}						t_expr_number;

typedef struct			s_expr_op
{
	t_expr_arg_type		type;
	t_token				token;
	union u_expr		*args;
	uint8_t				len;
	t_token_type		result_type;

	// Specifics
	t_expr_op_type		op;
}						t_expr_op;

/*
** =============================================================================
** 				Expressions structures
** =============================================================================
*/

typedef union			u_expr
{
	struct {
		t_expr_arg_type	type;
		t_token			token;
		union u_expr	*args;
		uint8_t			len;
		t_token_type	result_type;
	};
	t_expr_token		tkn;
	t_expr_op			op;
	t_expr_number		num;
}						t_expr;

/*
** =============================================================================
** 				Ast structures
** =============================================================================
*/
typedef enum			e_ast_node_type
{
	AST_TYPE_TOKEN,
	AST_TYPE_EXPR,
	AST_TYPE_INSTRUCTION,
	AST_TYPE_KEY,
	AST_TYPE_MAX
}						t_ast_node_type;

/*
** =============================================================================
** 				Ast arg substructures
** =============================================================================
*/

typedef struct			s_ast_token
{
	t_ast_node_type		type;
	t_token				token;
}						t_ast_token;

typedef struct			s_ast_expr
{
	t_ast_node_type		type;
	t_expr				expr;
	t_token_type		result_type;
	t_token				target;
}						t_ast_expr;

typedef struct			s_ast_instruction
{
	t_ast_node_type		type;

	t_token				*op;
	union u_ast_node	*args;
	size_t				len;
}						t_ast_instruction;

typedef struct			s_ast_key
{
	t_ast_node_type		type;

	char				*key;
	size_t				len;
	t_token				*token;
}						t_ast_key;

/*
** =============================================================================
** 				Ast arg structures
** =============================================================================
*/

typedef union			u_ast_node
{
	struct {
		t_ast_node_type	type;
	};
	t_ast_token			token;
	t_ast_expr			expr;
	t_ast_instruction	instruction;
	t_ast_key			key;
}						t_ast_node;

typedef struct			s_ast
{
	t_dynarray			tree;
	t_state				state;
	t_hashmap			keys;
	// TODO Add important ast properties here
}						t_ast;

typedef struct			s_ast_construct
{
	t_ast_node			node;
	t_state				state;
	bool				valid;
}						t_ast_construct;

/*
** =============================================================================
** 				Ast constructor
** =============================================================================
*/

t_ast					construct_ast(
		t_binary *binary,
		t_dynarray *tokens
);

/*
** =============================================================================
** 				Ast sub-constructors
** =============================================================================
*/

typedef struct			s_ast_constructor_param
{
		t_ast			*ast;
		t_binary		*binary;
		t_dynarray		*tokens;
		t_token			*input;
		uint64_t		*index;
}						t_ast_constructor_param;

typedef t_ast_construct	(t_ast_constructor)(
		t_ast_constructor_param
);

t_ast_constructor		construct_instruction;
t_ast_constructor		construct_property;
t_ast_constructor		construct_key;
t_ast_constructor		construct_expr;

/*
** =============================================================================
** 				Print ast internal functions
** =============================================================================
*/

void					print_ast_expr_tree_wrap(
		t_expr *expr,
		size_t scope
);

void					print_ast_node_tree_wrap(
		t_ast_node *node,
		size_t scope
);

/*
** =============================================================================
** 				Ast printing
** =============================================================================
*/

void					print_ast_node_tree(t_ast_node *node);
void					print_ast(t_ast *ast);

void					ast_destroy(t_ast *ast);

#endif
