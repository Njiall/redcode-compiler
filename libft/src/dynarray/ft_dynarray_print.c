/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dynarray_print.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/24 17:39:35 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/27 13:36:57 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "dynarray.h"

void				ft_dynarray_print(
		const t_dynarray * const arr
)
{
	uint8_t			*byte;
	uint64_t		i;

	i = 0;
	// Start of data struct
	printf("Dynarray [\e[1;34m%p\e[0m] \e[1;2;30m{\e[0m\n"
			"  offset\e[1;2m:\e[0m \e[32m%llu\e[0;2m,\e[0m\n"
			"  size\e[1;2m:\e[0m \e[32m%llu\e[0;2m,\e[0m\n"
			"  len\e[1;2m:\e[0m \e[32m%llu\e[0;2m,\e[0m\n"
			"  min\e[1;2m:\e[0m \e[32m%llu\e[0;2m,\e[0m\n"
			"  max\e[1;2m:\e[0m \e[32m%llu\e[0;2m,\e[0m\n"
			"  array [\e[1;34m%p\e[0m]\e[1;2m:\e[0m \e[1;2;30m{\e[0m", arr,
			arr->offset, arr->size, arr->index, arr->min_size, arr->max_size,
			arr->array
	);

	while ((byte = ft_dynarray_iterate(arr, &i, sizeof(uint8_t))))
	{
		// Appends a newline and print a diff on the line if necessary
		if ((i - 1) % 32 == 0) {
			printf("\e[0m\n");
			printf("    \e[0;1;34m0x%04llx\e[2m:\e[0m ", i - 1);
		}
		// Prints line header
		// Prints a byte
		if (!*byte)
			printf("\e[1;2;30m%02hhx\e[0m", *byte);
		else
			printf("\e[1m%02hhx\e[0m", *byte);
		if (i < arr->index && (i - 1) % 32 != 32 - 1)
			printf(" ");
	}
	printf("\n  }\n}\n");
}
