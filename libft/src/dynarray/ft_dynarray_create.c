/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dynarray_create.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/28 02:05:01 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/24 08:29:39 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "dynarray.h"

#include <stdlib.h>
#include <string.h>

/*
** Creates a dynarray with the size passed in the parameter.
** 		Returns a structure pointer `t_dynarray *`.
**
** min : min size of the array allocated.
** max : max exponential groth treshold of the array allocated.
*/
t_dynarray				*ft_dynarray_create(uint64_t min, uint64_t max)
{
	t_dynarray			*arr;

	if (min == 0)
		min = DYNARRAY_DEFAULT_SIZE;
	if (max == 0)
		max = DYNARRAY_DEFAULT_MAX;
	if (!(arr = calloc(1, sizeof(t_dynarray)))
		|| !(arr->array = malloc(min)))
	{
		if (arr)
			free(arr);
		ft_exit(1, ft_strajoin(4, __FILE__, ": ", __func__,
					": No space left on device.\n"), NULL);
	}
	arr->offset = min / 3;
	arr->index = 0;
	arr->size = min;
	arr->min_size = min;
	arr->max_size = max;
	return (arr);
}

/*
** Creates a dynarray with the size passed in the parameter.
** 		Returns a structure `t_dynarray`.
**
** min : min size of the array allocated.
** max : max exponential groth treshold of the array allocated.
*/
t_dynarray				ft_dynarray_create_loc(uint64_t min, uint64_t max)
{
	t_dynarray			arr;

	if (min == 0)
		min = DYNARRAY_DEFAULT_SIZE;
	if (max == 0)
		max = DYNARRAY_DEFAULT_MAX;
	arr = (t_dynarray){
		.array = malloc(min),
			.offset = min / 3,
			.index = 0,
			.size = min,
			.min_size = min,
			.max_size = max
	};
	if (!arr.array)
		ft_exit(1, ft_strajoin(4, __FILE__, ": ", __func__,
					": No space left on device.\n"), NULL);
	return (arr);
}

/*
** Clones a dynarray.
** 		Returns a structure pointer `t_dynarray`.
**
** dyn : original array to clone.
*/
t_dynarray				*ft_dynarray_clone(const t_dynarray * const dyn)
{
	t_dynarray			*new;

	if (!(new = ft_dynarray_create(dyn->min_size, dyn->max_size)))
		return (NULL);
	ft_dynarray_push(new, dyn->array, dyn->index);
	return (new);
}

/*
** Clones a dynarray.
** 		Returns a structure `t_dynarray`.
**
** dyn : original array to clone.
*/
t_dynarray				ft_dynarray_clone_loc(const t_dynarray * const dyn)
{
	t_dynarray			new;

	new = ft_dynarray_create_loc(dyn->min_size, dyn->max_size);
	ft_dynarray_push(&new, dyn->array, dyn->index);
	return (new);
}
