/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hashmap_create_custom.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/27 11:07:05 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/27 11:25:49 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "hashmap.h"
#include "libft.h"

t_hashmap				*ft_hashmap_create_custom(
		t_hash_type (*hash_function)(const void *, const size_t),
		size_t (*size_evaluator)(const void *)
)
{
	t_hashmap		*map;

	if (!(map = (t_hashmap*)malloc(sizeof(t_hashmap))))
		return (NULL);
	map->length = 0;
	map->hash_function = hash_function;
	map->size_evaluator = size_evaluator;
	ft_memset(map->table, 0, sizeof(*map->table) * HASHTABLE_SIZE);
	return (map);
}

t_hashmap				ft_hashmap_create_custom_loc(
		t_hash_type (*hash_function)(const void *, const size_t),
		size_t (*size_evaluator)(const void *)
)
{
	return ((t_hashmap){
			.hash_function = hash_function,
			.size_evaluator = size_evaluator,
			.length = 0
	});
}
