/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hashmap_create_default.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/31 05:06:48 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/27 11:52:01 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "hashmap.h"
#include "libft.h"

size_t				ft_hash_strlen(const void *str)
{
	return (ft_strlen((const char *)str));
}

/*
** This creates and initialize the hashmap.
** 		There is a trick there to test out. This is actually commented
** 		for testing reasons.
*/

t_hashmap			*ft_hashmap_create_default(void)
{
	t_hashmap		*map;

	if (!(map = (t_hashmap*)malloc(sizeof(t_hashmap))))
		return (NULL);
	map->length = 0;
	map->hash_function = &ft_hash_djb2a;
	map->size_evaluator = &ft_hash_strlen;
	ft_memset(map->table, 0, sizeof(*map->table) * HASHTABLE_SIZE);
	return (map);
}

t_hashmap			ft_hashmap_create_default_loc(void)
{
	return ((t_hashmap){
			.hash_function = &ft_hash_djb2a,
			.size_evaluator = &ft_hash_strlen,
			.length = 0
	});
}
