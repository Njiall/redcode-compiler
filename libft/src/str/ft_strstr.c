/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/18 05:31:00 by mbeilles          #+#    #+#             */
/*   Updated: 2019/09/04 10:13:52 by njiall           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"

static inline size_t	*ft_preproc(
		const char *needle,
		const size_t nlen
)
{
	static size_t		table[256];
	size_t				i;

	i = SIZE_MAX;
	while (++i < 256)
		table[i] = nlen;
	i = SIZE_MAX;
	while (++i < nlen - 1)
		table[(unsigned char)needle[i]] = nlen - 1 - i;
	return (table);
}

const char				*ft_strstr(
		const char *haystack,
		const char *needle
)
{
	size_t				*table;
	size_t				offset;
	size_t				hlen;
	size_t				nlen;
	size_t				i;

	hlen = ft_strlen(haystack);
	nlen = ft_strlen(needle);
	table = ft_preproc(needle, nlen);
	offset = 0;
	while (hlen - offset >= nlen)
	{
		i = nlen - 1;
		while (haystack[offset + i] == needle[i])
			if (i == 0)
				return (haystack + offset);
			else
				--i;
		offset += table[(unsigned char)haystack[offset + nlen - 1]];
	}
	return (NULL);
}
